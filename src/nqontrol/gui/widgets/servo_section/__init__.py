from .autolockWidget import AutoLockWidget
from .servoSwitchesWidget import ServoSwitchesWidget
from .servoWidget import ServoWidget
