'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 1000
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 6.2.0
' Optimize                       = Yes
' Optimize_Level                 = 3
' Stacksize                      = 1000
' Info_Last_Save                 = VBOX  vBox\Chris
'<Header End>
#Include ADwinPro_All.inc
#Include .\filter_module.inc
Import Math.lic

#Define AIN_MODULE_1 1
#Define AIN_MODULE_2 2
#Define AOUT_MODULE_1 3
#Define AOUT_MODULE_2 4
#Define NUM_INPUT_CHANNELS 8
#Define NUM_OUTPUT_CHANNELS 8
#Define NUM_ADC_CHANNELS 16
#Define NUM_DAC_CHANNELS 16
#Define CPU_CLK 1000000000
#Define PROC_CLK 200000
#Define FIFO_BUFFER_SIZE 10003
#Define TTL_HIGH 0C000h  ' +5V
#Define TTL_LOW 8000h   ' 0V

#Define timer PAR_1
' monitor control Parameter, values from 1 to 8
#Define monitor_sel Data_6
Dim monitor_sel[NUM_SERVOS] as Long
' TTL
Dim TTL_current[NUM_SERVOS] as Long
#Define TTL_DIG_CHANNEL 0

Dim timer_last, timer_now, t1, t2, new_time As Long

' The inputs straight from the ADC
Dim adc_input[NUM_ADC_CHANNELS] As Long
' The outputs straight into the DAC
Dim dac_output[NUM_DAC_CHANNELS] As Long

#Define input_sensitivity Par_8
Dim input_sensitivity_last as Long

#Define fifo_data Data_3
#Define fifo_sel Par_4
#Define fifo_counter_max Par_6
Dim fifo_data[FIFO_BUFFER_SIZE] as Float as FIFO
Dim fifo_counter = 1 as Long at Dm_Local

#Define last_output Data_4
Dim last_output[NUM_SERVOS] as Long

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' autolock fields
#Define lockControl Par_19  ' the state parameters, three states written on different bits of an integer
#Define lockControlPlus Par_20  ' contains the bit switches for lockGreater (first 8 bits) and lockRampmode (bit 9-16)
#Define NUM_LOCK_INDICES 32
#Define LOCK Data_8  ' the lock voltage values as floats: threshold, threshold_break, amplitude and offset:
Dim LOCK[NUM_LOCK_INDICES] as Float  ' will contain threshold, threshold_breal amplitude and offset of the ramp
#Define lockIter Data_9
Dim lockIter[NUM_SERVOS] as Float  ' iteration variable, will be between -1 to 1, runs on the normed ramp
#Define lockStep Data_11
Dim lockStep[NUM_SERVOS] as Float  ' stepsize
Dim lockStepSign[NUM_SERVOS] as Long  ' iteration step sign, either +1 or -1
Dim lockPosition[NUM_SERVOS] as Float  ' this is the actual Iterator value that gets added to the output and uses something like: amplitude * lockIter + offset
Dim lockBuffer[NUM_SERVOS] as Long  ' Whenever a lock is found we want some downtime on the relock/break feature (this was related to some strange behaviour on the ADwin)
#Define testOutput Data_10  ' expose the last output used IN THE LOCK for testing, different from last_output above
Dim testOutput[NUM_SERVOS] as Long


Dim i as long
Dim ADCF_Mode as Long


Sub Blinkenlights()
  Dim even_odd as Long
  timer_now = Read_Timer()
  If ((timer_now - timer_last) > CPU_CLK) Then
    Inc(timer)
    even_odd = Mod(timer, 2)
    P2_Set_Led(AIN_MODULE_1, even_odd)
    P2_Set_Led(AOUT_MODULE_1, even_odd)
    P2_Set_Led(AIN_MODULE_2, 1-even_odd)
    P2_Set_Led(AOUT_MODULE_2, 1-even_odd)
    timer_last = timer_now
  EndIf
EndSub

Sub SetInputSensitivity()
  Dim inputMode as long
  Dim auxMode as long

  if (input_sensitivity_last <> input_sensitivity) then
    For i=1 to NUM_INPUT_CHANNELS
      inputMode = Shift_Right(input_sensitivity And Shift_Left(3, 2*i-2), 2*i-2)
      auxMode = Shift_Right(input_sensitivity And Shift_Left(3, 2*i+14), 2*i+14)

      P2_Set_Gain(AIN_MODULE_1, i, inputMode)
      P2_Set_Gain(AIN_MODULE_2, i, auxMode)
    next i
    input_sensitivity_last = input_sensitivity
  endif
EndSub

Sub SendToMonitors()
  Dim mon as Long
  for mon=1 to NUM_INPUT_CHANNELS
    If (monitor_sel[mon] > 0) Then
      If (monitor_sel[mon] > 20) Then
        If (monitor_sel[mon] < 29) Then
          ' Output signal
          dac_output[mon+NUM_OUTPUT_CHANNELS] = dac_output[monitor_sel[mon]-20]
        Else
          ' TTL signal
          dac_output[mon+NUM_OUTPUT_CHANNELS] = TTL_current[monitor_sel[mon]-30]
        EndIf
      Else
        ' Input and aux signal
        dac_output[mon+NUM_OUTPUT_CHANNELS] = adc_input[monitor_sel[mon]]
      EndIf
    EndIf
  next mon
EndSub

Sub SendToFifo()
  Dim combined as Float
  If (fifo_sel > 0 ) Then
    If (fifo_counter >= fifo_counter_max) Then ' There is a `&&` or `AndAlso` operator missing...
      ' send values for current channel to PC
      ' Note: we do this before filters, as filters do calculations on adc_input
      ' We want to ensure a fixed phase between the channels.
      ' Therefore we combine all in one fifo array field.

      ' Saving 3 16bit channels in a 64bit long variable
      ' Byte    | 7 6 | 5 4   | 3 2 | 1 0    |
      ' Channel |     | input | aux | output |

      combined = (adc_input[fifo_sel]) * 4294967296 ' This number is 2^32 and is a workaround for Shift_Left, which is not working for results > 32bit
      combined = combined + Shift_Left(adc_input[fifo_sel+8], 16)
      combined = combined + dac_output[fifo_sel]

      fifo_data = combined

      fifo_counter = 0
    EndIf
    fifo_counter = fifo_counter + 1
  EndIf
EndSub

Sub ClearFilterHistory(servo_channel)
  Dim x as Long
  ' TOTAL_HISTORY is 80, NUM_SERVOS * NUM_SOS * NUM_HISTORY (8 * 5 * 2) (see filter_module.inc)
  ' relevant index is idx = sos_index + servo_index * NUM_SOS
  ' so idx = sos_index + (channel-1) * NUM_SOS
  ' sos_index runs from 0 to 4 (5-1)
  ' filter_history[(sos_index + (channel-1) * NUM_SOS) * NUM_HISTORY + 1] bzw +2
  ' so, low end is filter_history[(channel - 1) * NUM_SOS * NUM_HISTORY + 1]
  ' upper is filter_history[(4 + (channel - 1) * NUM_SOS) * NUM_HISTORY + 2]
  ' since we do not want to recalculate this all the time, we fill in NUM_SOS and NUM_HISTORY, so
  ' so, low end is filter_history[(channel - 1) * 5 * 2 + 1]
  ' upper is (4 + (channel - 1) * 5) * 2 + 2 = 10 + (channel-1) * 10 =  channel * 10
  for x = ((servo_channel - 1) * 10 + 1) to (10 * servo_channel)
    filter_history[x] = 0.0  ' filter history variable is also defined in filter_module.inc
  next x
EndSub

Sub TurnOffInputOutput(servo_channel)
  ' switch bits are defined in filter_module.inc
  par[10 + servo_channel] = (par[10 + servo_channel] And Not(FCR_SW_INPUT))
  par[10 + servo_channel] = (par[10 + servo_channel] And Not(FCR_SW_OUTPUT))
EndSub

Sub TurnOffAux(servo_channel)
  ' switch bits are defined in filter_module.inc
  par[10 + servo_channel] = (par[10 + servo_channel] And Not(FCR_SW_AUX))
EndSub

Sub TurnOnInputOutput(servo_channel)
  ' fcr_1-8 corresponts to PAR_11-18
  par[10 + servo_channel] = (par[10 + servo_channel] Or FCR_SW_INPUT)
  par[10 + servo_channel] = (par[10 + servo_channel] Or FCR_SW_OUTPUT)
EndSub

Sub AutoLock()
  ' variables only serve as namespace and readability, are reassigned every cycle
  Dim channel as Long  ' servo looping variable, which channel
  Dim search as Long  ' to store the lock state (on/off)
  Dim locked as Long
  Dim relock as Long  ' whether relock is active
  Dim threshold as Float  ' threshold value
  Dim threshold_break as Float  ' this is the value where `locked` mode will be broken, the upper/lower bound of the corresponding threshold
  Dim greater as Long  ' direction of the threshold passing
  Dim rampmode as Long
  Dim amplitude as Float
  Dim offset as Float
  Dim aux as Long  ' gonna hold the aux input value
  Dim output as Long  ' gonna hold the output value

  ' NOTE: there is also lockIter for each servo, which is stored in ADwin Data fields, see start of document

  for channel=1 to NUM_SERVOS  ' let's do this for every servo
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' lock states from the control register
    search = lockControl and Shift_Left(1, (channel-1))
    locked = lockControl and Shift_Left(1, (channel-1) + 8)
    relock = lockControl and Shift_Left(1, (channel-1) + 16)
    ' the direction from the separate parameter
    greater = lockControlPlus and Shift_Left(1, channel - 1)
    rampmode = lockControlPlus and Shift_Left(1, channel - 1 + NUM_SERVOS)
    ' lock values
    threshold = LOCK[channel]
    threshold_break = LOCK[channel + 8]
    amplitude = LOCK[channel + 16]
    offset = LOCK[channel + 24]

    ' aux signal
    aux = adc_input[channel + NUM_SERVOS]
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Count down the buffer
    If (lockBuffer[channel] > 0) Then
      lockBuffer[channel] = lockBuffer[channel] -1
    EndIf

    ' LOCK-OFF STATE
    ' simplest case, autolocking is off
    If ((search = 0) And (locked = 0)) Then
      lockIter[channel] = -2.00
    EndIf

    ' LOCK SEARCH STATE
    If (search > 0) Then
      lockControl = lockControl And Not(Shift_Left(1, (channel-1) + 8))  ' set the locked bit to 0
      ' Turn off input and output channels when starting new search ( not sure this is necessary)
      If (lockIter[channel] = -2.00) Then  ' the -2 is basically a reset indicator, or rather a "i havent started yet indicator"
        lockIter[channel] = 0.00  ' start the iterator at 0
        ' turn off the control channels, so lock can search freely
        TurnOffInputOutput(channel)
        TurnOffAux(channel)
        ' before locking, we want to reset the filter history of our second order sections
        ClearFilterHistory(channel)
      EndIf

      ' Different cases depending on whether peaks are expected as maxima or minima
      ' Basically the peak direction: If the peak is a minimum, we want to check whether the aux signal falls below the threshold, in case of a maximum, we want the aux to surpass the threshold

      If ((rampmode = 0) And (((greater > 0) And (aux > threshold)) Or ((greater = 0) And (aux < threshold)))) Then  ' LOCK IS FOUND
        lockControl = lockControl And Not(Shift_Left(1, channel-1))  ' set the lock search bit to 0
        lockControl = lockControl Or Shift_Left(1, (channel-1) + 8)  ' set the locked bit to 1
        ' set lock buffer to a 10000 cycles (.1 seconds)
        lockBuffer[channel] = 10000

        ' activate filters/activate input
        TurnOnInputOutput(channel)
      Else ' SEARCHING FOR PEAK
        ' adjust the iterator direction at maximum/minimum of ramp
        ' minimum
        If (lockIter[channel] <= -1) Then
          lockStepSign[channel] = 1
          TTL_current[channel] = TTL_HIGH  ' sets the oscilloscope trigger
          CPU_Digout(TTL_DIG_CHANNEL, 1)  ' TTL high
        Else
          TTL_current[channel] = TTL_LOW  ' sets the oscilloscope trigger
          CPU_Digout(TTL_DIG_CHANNEL, 0)  ' TTL low
        EndIf
        ' maximum
        If (lockIter[channel] >= 1) Then  ' max range is reached
          lockStepSign[channel] = -1
        EndIf

        lockIter[channel] = lockIter[channel] + lockStepSign[channel] * lockStep[channel]
      EndIf

      ' calculate the new lockposition based on the current iterator
      lockPosition[channel] = amplitude * (lockIter[channel] * 8000h / 10) + offset
      ' send it to the output
      dac_output[channel] = round(lockPosition[channel])  ' round this to avoid conversion errors because the output is a Long
    EndIf

    ' if in LOCKED state
    If ((locked > 0)) Then
      ' check whether the threshold conditions are still met, but only after the lockBuffer has counted to 0 (to give the filters some time to kick in)
      If (((((greater > 0) And (aux < threshold_break)) Or ((greater = 0) And (aux > threshold_break))) And (lockBuffer[channel] = 0)) Or (rampmode > 0)) Then  ' lock is broken, use threshold_break
        If ((relock > 0) Or (rampmode > 0)) Then
          lockControl = lockControl Or Shift_Left(1, (channel - 1))  ' set lock search state to searching again
        Else
          lockIter[channel] = -2  ' reset lock iterator
        EndIf
        lockControl = lockControl And Not(Shift_Left(1, (channel - 1) + 8))  ' set the locked bit to 0

      EndIf
      ' regardless of breaking or not, set the output (so it will adjust output in bufferphase)
      ' add the control signal on top of the lock value
      dac_output[channel] = round(lockPosition[channel]) + dac_output[channel] - 8000h
      ' AN ADDITIONAL READOUT ONLY FOR TESTING PURPOSES (IGNORE)
      testOutput[channel] = dac_output[channel]  ' in servo.py see _testLockOutput for more information
    EndIf

  next channel
EndSub

Sub LimitToOutputRange()
  Dim iij as Long
  'setting values which are above 65535 or below 0 to the maximum(65535) respectively minimum value(0) of the P2_Write_DAC8 command,
  'since it only takes the last 16 bits of integers out of its boundaries
  for iij=1 to NUM_DAC_CHANNELS
    if (dac_output[iij]>65535) then
      dac_output[iij]=65535
    endif
    if (dac_output[iij]<0) then
      dac_output[iij]=0
    endif
  next iij
EndSub

Sub UpdateLastOutput()
  ' just here for testing
  ' in case you want to read out not the full FIFO, but just the last value
  for i=1 to NUM_SERVOS
    last_output[i] = dac_output[i]
  next i
  ' Alternative:
  ' P2_Write_DAC8(last_output, dac_output, 1)
EndSub


Init:
  Processdelay = CPU_CLK/PROC_CLK ' 200 KHz
  'setting the values of the filter control register to zero
  Par_11=0
  Par_12=0
  Par_13=0
  Par_14=0
  Par_15=0
  Par_16=0
  Par_17=0
  Par_18=0
  ' lock control register
  PAR_19=0
  ' lock greater
  PAR_20=0
  'prepoluate dac_output

  input_sensitivity = 0
  input_sensitivity_last = 1
  timer = 0
  timer_last = Read_Timer()

  Fifo_Clear(3)

  CPU_Dig_IO_Config(110011b)

  ' TODO nicer?
  for i=1 to 8
    monitor_sel[i] = 0
    lockIter[i] = -2  ' init locking iterator on 0
    lockStep[i] = 1.0
    lockPosition[i] = 0.0
    lockStepSign[i] = 1
    testOutput[i] = 0
    lockBuffer[i] = 0
    LOCK[i] = 0.00  ' autolock threshold values
    LOCK[i + 8] = 0.00 ' threshold break values
    LOCK[i + 16] = 10.00  ' autolock amplitude values
    LOCK[i + 24] = 8000h  ' autolock offset values
  next i

  P2_Set_Average_Filter(AIN_MODULE_1, 0) ' use 5 to average 32 measurements

  ' Pseudo code:
  ' ADCF_Mode = 2^(AIN_MODULE_1-1) + 2^(AIN_MODULE_2-1)
  ADCF_Mode = Shift_Left(1, AIN_MODULE_1-1) + Shift_Left(1, AIN_MODULE_2-1)

  P2_ADCF_Mode(ADCF_Mode, 1)   ' Enable Timer mode on Module 2 & 3 (bit flag for module address!)


Event:
  t1 = Read_Timer()

  ' Start DAC conversion on all DACs simultaneously
  P2_Sync_All(Shift_Left(1, AOUT_MODULE_1-1) Or Shift_Left(1, AOUT_MODULE_2-1))

  ' Read in values from ADC modules, these have already been converted because of the
  ' ADCF timer mode
  P2_Read_ADCF8(AIN_MODULE_1, adc_input, 1)
  P2_Read_ADCF8(AIN_MODULE_2, adc_input, 9)

  SendToMonitors()

  SendToFifo()

  RunFilters(adc_input, dac_output)

  AutoLock()  ' has to be before limiting output range but after RunFilters

  UpdateLastOutput()

  LimitToOutputRange()

  ' pre-populate DACs with new values
  P2_Write_DAC8(AOUT_MODULE_1, dac_output, 1)
  P2_Write_DAC8(AOUT_MODULE_2, dac_output, 9)

  Blinkenlights()

  SetInputSensitivity()

  t2 = Read_Timer()
  new_time = t2 - t1 -4
  Par_7 = new_time

Finish:
  ' Switch off LEDs
  P2_Set_Led(AIN_MODULE_1, 0)
  P2_Set_Led(AIN_MODULE_2, 0)
  P2_Set_Led(AOUT_MODULE_1, 0)
  P2_Set_Led(AOUT_MODULE_2, 0)
