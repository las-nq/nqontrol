'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 1000
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 6.2.0
' Optimize                       = Yes
' Optimize_Level                 = 3
' Stacksize                      = 1000
' Info_Last_Save                 = VBOX  vBox\Chris
' Foldings                       = 65,79,95,115
'<Header End>
#Include ADwinPro_All.inc
#Include .\filter_module.inc
Import Math.lic

#Define AIN_MODULE_1 1
#Define AIN_MODULE_2 2
#Define AOUT_MODULE_1 3
#Define AOUT_MODULE_2 4
#Define NUM_INPUT_CHANNELS 8
#Define NUM_OUTPUT_CHANNELS 8
#Define NUM_ADC_CHANNELS 16
#Define NUM_DAC_CHANNELS 16
#Define CPU_CLK 1000000000
#Define PROC_CLK 200000
#Define FIFO_BUFFER_SIZE 10003
#Define NUM_RAMP_VALUES 131072
#Define TTL_HIGH 0C000h  ' +5V
#Define TTL_LOW 8000h   ' 0V

#Define timer PAR_1
' monitor control Parameter, values from 1 to 8
#Define monitor_sel Data_6
Dim monitor_sel[NUM_FILTERS] as Long
' TTL
Dim TTL_current as Long
#Define TTL_DIG_CHANNEL 0
' Ramp Control Register
#Define rcr Par_3
' Ramp amplitude. 1 means ±10V
#Define ramp_amplitude FPar_1
'ramp Data array
Dim ramp_data[NUM_RAMP_VALUES], ramp_idx, ramp_chan as Long

Dim timer_last, timer_now, t1, t2, new_time As Long

' The inputs straight from the ADC
Dim adc_input[NUM_ADC_CHANNELS] As Long
' The outputs straight into the DAC
Dim dac_output[NUM_DAC_CHANNELS] As Long

#Define input_sensitivity Par_8
Dim input_sensitivity_last as Long

#Define fifo_data Data_3
#Define fifo_sel Par_4
#Define fifo_counter_max Par_6
Dim fifo_data[FIFO_BUFFER_SIZE] as Float as FIFO
Dim fifo_counter = 1 as Long at Dm_Local

#Define last_output Data_4
Dim last_output[NUM_FILTERS] as Long

' autolock stuff
#Define LOCK Data_8
#Define NUM_INDICES 40  ' change this later on
Dim LOCK[NUM_INDICES] as Long
Dim lockIter[NUM_FILTERS] as Long
#Define threshold_value_mask 0ffffh
#Define threshold_direction 10000h  ' Look for signal lower (0) or greater (1) than the threshold
Dim LOCK_COUNTER[NUM_FILTERS] as Long
#Define lock_mask 3h
#Define relock_mask 4h

Dim i as long
Dim ADCF_Mode as Long


Sub Blinkenlights()
  Dim even_odd as Long
  timer_now = Read_Timer()
  If ((timer_now - timer_last) > CPU_CLK) Then
    Inc(timer)
    even_odd = Mod(timer, 2)
    P2_Set_Led(AIN_MODULE_1, even_odd)
    P2_Set_Led(AOUT_MODULE_1, even_odd)
    P2_Set_Led(AIN_MODULE_2, 1-even_odd)
    P2_Set_Led(AOUT_MODULE_2, 1-even_odd)
    timer_last = timer_now
  EndIf
EndSub

Sub SetInputSensitivity()
  Dim inputMode as long
  Dim auxMode as long

  if (input_sensitivity_last <> input_sensitivity) then
    For i=1 to NUM_INPUT_CHANNELS
      inputMode = Shift_Right(input_sensitivity And Shift_Left(3, 2*i-2), 2*i-2)
      auxMode = Shift_Right(input_sensitivity And Shift_Left(3, 2*i+14), 2*i+14)

      P2_Set_Gain(AIN_MODULE_1, i, inputMode)
      P2_Set_Gain(AIN_MODULE_2, i, auxMode)
    next i
    input_sensitivity_last = input_sensitivity
  endif
EndSub

Sub SendToMonitors()
  Dim mon as Long
  for mon=1 to NUM_INPUT_CHANNELS
    If (monitor_sel[mon] > 0) Then
      If (monitor_sel[mon] > 20) Then
        If (monitor_sel[mon] < 29) Then
          ' Output signal
          dac_output[mon+NUM_OUTPUT_CHANNELS] = dac_output[monitor_sel[mon]-20]
        Else
          ' TTL signal
          dac_output[mon+NUM_OUTPUT_CHANNELS] = TTL_current
        EndIf
      Else
        ' Input and aux signal
        dac_output[mon+NUM_OUTPUT_CHANNELS] = adc_input[monitor_sel[mon]]
      EndIf
    EndIf
  next mon
EndSub

Sub SendToFifo()
  Dim combined as Float
  If (fifo_sel > 0 ) Then
    If (fifo_counter >= fifo_counter_max) Then ' There is a `&&` or `AndAlso` operator missing...
      ' send values for current channel to PC
      ' Note: we do this before filters, as filters do calculations on adc_input
      ' We want to ensure a fixed phase between the channels.
      ' Therefore we combine all in one fifo array field.

      ' Saving 3 16bit channels in a 64bit long variable
      ' Byte    | 7 6 | 5 4   | 3 2 | 1 0    |
      ' Channel |     | input | aux | output |

      combined = (adc_input[fifo_sel]) * 4294967296 ' This number is 2^32 and is a workaround for Shift_Left, which is not working for results > 32bit
      combined = combined + Shift_Left(adc_input[fifo_sel+8], 16)
      combined = combined + dac_output[fifo_sel]

      fifo_data = combined

      fifo_counter = 0
    EndIf
    fifo_counter = fifo_counter + 1
  EndIf
EndSub

Sub RunRamp()
  ramp_chan = rcr And 0fh
  If (ramp_chan > 0) Then
    dac_output[ramp_chan] = (ramp_data[ramp_idx] * ramp_amplitude) + 8000h
    ramp_idx = ramp_idx + Shift_Right(rcr And 0ff00h, 8)
    If (ramp_idx > NUM_RAMP_VALUES) Then
      ramp_idx = 1
      CPU_Digout(TTL_DIG_CHANNEL, 1)  ' TTL high
      TTL_current = TTL_HIGH
    Else
      CPU_Digout(TTL_DIG_CHANNEL, 0)  ' TTL low
      TTL_current = TTL_LOW
    EndIf
  EndIf
EndSub

Sub AutoLock()
  Dim channel as Long  ' servo looping variable
  Dim indexoffset as Long  ' variable for array index offset
  Dim state as Long
  Dim relock as Long
  Dim threshold as Long
  Dim greater as Long
  Dim offset as Long
  Dim searchStart as Long
  Dim searchEnd as Long
  Dim aux as Long

  for channel=1 to NUM_FILTERS
    indexoffset = (channel - 1) * 5  ' each servo occupies 5 channels currently
    state = (LOCK[1 + indexoffset] And lock_mask)
    relock = (LOCK[1 + indexoffset] And relock_mask)
    threshold = (LOCK[2 + indexoffset] And threshold_value_mask)
    greater = (LOCK[2 + indexoffset] And threshold_direction)
    offset = LOCK[5 + indexoffset]  ' this is the voltage that is added onto the output after a peak was found and fitlers have been activated
    searchStart = LOCK[3 + indexoffset]
    searchEnd = LOCK[4 + indexoffset]
    aux = adc_input[channel + NUM_FILTERS]

    ' LOCK-OFF STATE
    If (state = 0) Then
      lockIter[channel] = 0
      LOCK[5 + indexoffset] = 0  ' locking offset to 0 in case lock is off
    EndIf

    ' LOCK STATE
    If (state = 2) Then
      ' add offset (lastFound LOCK[5 + offset]) to dac_output after filters have been applied
      dac_output[channel] = dac_output[channel] + offset
      If (LOCK_COUNTER[channel] > 0) Then
        LOCK_COUNTER[channel] = LOCK_COUNTER[channel] - 1
      Else
        ' Different cases depending on whether peaks are expected as maxima or minima
        ' For a maximum, we want to check whether the aux falls below the threshold before triggering a reload, for a minimum, we want to check whether it is above the threshold
        If ((greater > 0) And (aux < threshold * 0.5)) Then
          ' If (relock > 0) Then
          '   LOCK[1 + indexoffset] = 1 + relock ' trigger relock in case aux is below threshold and direction is set to 'greater'
          ' Else
          LOCK[1 + indexoffset] = 0
          ' EndIf
        Else
          If ((greater = 0) And(aux > threshold * 0.5)) Then
            ' If (relock > 0) Then
            '   LOCK[1 + indexoffset] = 1 + relock ' trigger relock in case aux is above threshold and direction is set to 'lesser'
            ' Else
            LOCK[1 + indexoffset] = 0
            ' EndIf
          EndIf
        EndIf
      EndIf

    EndIf

    ' LOCK SEARCH STATE
    If (state = 1) Then
      ' the 0 is basically a reset indicator, or rather a "i havent started yet indicator"
      If (lockIter[channel] = 0) Then
        lockIter[channel] = searchStart  ' this is the search minimum
        Dim x as Long
        ' relevant index is sos_index + filter_index * num_SOS
        ' sos index von 0 bis 4 (5-1)
        ' filter_index von 0 bis 7
        ' filter_history[idx*NUM_HISTORY+1] bzw +2
        ' also zb channel 1, bei NUM_SOS=5 hat dann myindices: [0-4] + 0 * 5, also 0-4
        ' und NUMHISTORY = 2, also hisotry[0-4 * 2 +1/2], also 0 bis 10
        ' what about filter_coeffs?
        for x = ((channel-1) * 10) to (10 * channel)
          filter_history[x] = 0.0
        next x
      EndIf

      ' Different cases depending on whether peaks are expected as maxima or minima
      ' Basically the peak direction: If the peak is a minimum, we want to check whether the aux signal falls below the threshold, in case of a maximum, we want the aux to surpass the threshold

      If (((greater > 0) And (aux > threshold)) Or ((greater = 0) And (aux < threshold))) Then  ' LOCK IS FOUND
        LOCK[1 + indexoffset] = 2  ' set the new lock state
        LOCK[5 + indexoffset] = lockIter[channel]  ' save the current voltage value as output offset
        LOCK_COUNTER[channel] = 100
        ' activate filters/activate input
        ' fcr_1-8 corresponts to PAR_11-18
        par[10 + channel] = (par[10 + channel] Or FCR_SW_INPUT)
        par[10 + channel] = (par[10 + channel] Or FCR_SW_OUTPUT)
        ' reset the iterator if threshold was breached, so that on next iteration/or when restarting it will be set to "searchStart" again
        lockIter[channel] = 0
      Else ' SEARCHING FOR PEAK
        ' Turn off input and output channels when starting new search
        If (lockIter[channel] = searchStart) Then
          par[10 + channel] = (par[10 + channel] And Not(FCR_SW_INPUT))
          par[10 + channel] = (par[10 + channel] And Not(FCR_SW_OutPUT))
        EndIf

        If (lockIter[channel] >= searchEnd) Then  ' max range is reached
          lockIter[channel] = searchStart  ' restart scan if maximum is reached
          TTL_current = TTL_HIGH
        Else
          lockIter[channel] = lockIter[channel] + 1
          TTL_current = TTL_LOW
        EndIf
      EndIf

      ' makes sense to move the actual output to the end, as the voltage will only apply on the next cycle anyway
      dac_output[channel] = lockIter[channel]
    EndIf

    next channel
EndSub

Sub LimitToOutputRange()
  Dim iij as Long
  'setting values which are above 65535 or below 0 to the maximum(65535) respectively minimum value(0) of the P2_Write_DAC8 command,
  'since it only takes the last 16 bits of integers out of its boundaries
  for iij=1 to NUM_DAC_CHANNELS
    if (dac_output[iij]>65535) then
      dac_output[iij]=65535
    endif
    if (dac_output[iij]<0) then
      dac_output[iij]=0
    endif
  next iij
EndSub

Sub UpdateLastOutput()
  for i=1 to NUM_FILTERS
    last_output[i] = dac_output[i]
  next i
  ' Alternative:
  ' P2_Write_DAC8(last_output, dac_output, 1)
EndSub


Init:
  Processdelay = CPU_CLK/PROC_CLK ' 200 KHz
  'setting the values of the filter control register to zero
  Par_11=0
  Par_12=0
  Par_13=0
  Par_14=0
  Par_15=0
  Par_16=0
  Par_17=0
  Par_18=0
  'prepoluate dac_output

  input_sensitivity = 0
  input_sensitivity_last = 1
  timer = 0
  timer_last = Read_Timer()
  ramp_chan = 0

  Fifo_Clear(3)

  CPU_Dig_IO_Config(110011b)

  ' TODO nicer?
  for i=1 to 8
    monitor_sel[i] = 0
  next i

  'populates ramp_data with the values from -10V to 10V
  For ramp_idx = 0 To 0ffffh
    ramp_data[ramp_idx+1] = ramp_idx - 8000h
    ramp_data[20000h-ramp_idx] = ramp_data[ramp_idx+1]
  Next ramp_idx
  ramp_idx = 1

  P2_Set_Average_Filter(AIN_MODULE_1, 0) ' use 5 to average 32 measurements

  ' Pseudo code:
  ' ADCF_Mode = 2^(AIN_MODULE_1-1) + 2^(AIN_MODULE_2-1)
  ADCF_Mode = Shift_Left(1, AIN_MODULE_1-1) + Shift_Left(1, AIN_MODULE_2-1)

  P2_ADCF_Mode(ADCF_Mode, 1)   ' Enable Timer mode on Module 2 & 3 (bit flag for module address!)


Event:
  t1 = Read_Timer()

  ' Start DAC conversion on all DACs simultaneously
  P2_Sync_All(Shift_Left(1, AOUT_MODULE_1-1) Or Shift_Left(1, AOUT_MODULE_2-1))

  ' Read in values from ADC modules, these have already been converted because of the
  ' ADCF timer mode
  P2_Read_ADCF8(AIN_MODULE_1, adc_input, 1)
  P2_Read_ADCF8(AIN_MODULE_2, adc_input, 9)

  SendToMonitors()

  SendToFifo()

  RunFilters(adc_input, dac_output)

  AutoLock()  ' has to be before limiting output range but after RunFilters

  RunRamp()

  LimitToOutputRange()

  UpdateLastOutput()

  ' pre-populate DACs with new values
  P2_Write_DAC8(AOUT_MODULE_1, dac_output, 1)
  P2_Write_DAC8(AOUT_MODULE_2, dac_output, 9)

  Blinkenlights()

  SetInputSensitivity()

  t2 = Read_Timer()
  new_time = t2 - t1 -4
  Par_7 = new_time

Finish:
  ' Switch off LEDs
  P2_Set_Led(AIN_MODULE_1, 0)
  P2_Set_Led(AIN_MODULE_2, 0)
  P2_Set_Led(AOUT_MODULE_1, 0)
  P2_Set_Led(AOUT_MODULE_2, 0)
