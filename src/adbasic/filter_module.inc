'<ADbasic Header, Headerversion 001.001>
'<Header End>
'<ADbasic Header, Headerversion 001.001>
'<Header End>
' *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
' IMPLEMENTATION OF INFINITE IMPULSE RESPONSE FILTERS
'
' There are NUM_FILTERS individual filters, where each filter takes
' ADC input (FILTER_NUM*2)-1 (so that there is always one corresponding
' input free for e.g. DC measurements), and passes it through up to
' 5 second-order sections. Output is written to DAC output FILTER_NUM.
'
' (c) 2017, S. Steinlechner, LAS-NQ, ILP, University of Hamburg
' *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


' CAUTION, NEED TO UPDATE ASSIGNMENT OF Par_xx as well
' if NUM_FILTER is changed!
#Define NUM_FILTERS       8    ' total number of filter modules
#Define NUM_SOS           5    ' second-order sections (SOS) per filter module
#Define NUM_COEFFS        5    ' filter coefficients per SOS
#Define NUM_HISTORY       2    ' number of history items for each SOS
#Define TOTAL_COEFFS      200  'NUM_FILTERS * NUM_SOS * NUM_COEFFS
#Define TOTAL_HISTORY     80   'NUM_FILTERS * NUM_SOS * NUM_HISTORY

'BUGBUG: first start after boot of ADwin always has more delay?!
'TODO: summing function for transfer function measurements

' Filter Bank Control Register
#Define fbcr Par_2
' Flags for Filter Bank Control Register
#Define FBCR_RELOAD_1  1
#Define FBCR_RELOAD_2  2
#Define FBCR_RELOAD_3  4
#Define FBCR_RELOAD_4  8
#Define FBCR_RELOAD_5  16
#Define FBCR_RELOAD_6  32
#Define FBCR_RELOAD_7  64
#Define FBCR_RELOAD_8  128

' Filter Control Registers
#Define fcr_1 Par_11
#Define fcr_2 Par_12
#Define fcr_3 Par_13
#Define fcr_4 Par_14
#Define fcr_5 Par_15
#Define fcr_6 Par_16
#Define fcr_7 Par_17
#Define fcr_8 Par_18
' Flags for the Filter Control Registers
#Define FCR_SW_INPUT   1
#Define FCR_SW_OUTPUT  2
#Define FCR_SW_OFFSET  4
' #Define FCR_SW_SNAP    8
#Define FCR_SW_SOS1    16
#Define FCR_SW_SOS2    32
#Define FCR_SW_SOS3    64
#Define FCR_SW_SOS4    128
#Define FCR_SW_SOS5    256
#Define FCR_SW_AUX     512

' Snap control
' #Define snap_config Data_7
' Dim snap_config[NUM_FILTERS] as Long
' #Define snap_value_mask 0ffffh
' #Define snap_lg 10000h  ' Look for signal lower (0) or greater (1) than the threshold

' Global storage of filter coefficient for transfer from PC
#Define filter_coeffs_PC Data_1
#Define offset_gain_PC Data_2
Dim filter_coeffs_PC[TOTAL_COEFFS] as Float
Dim offset_gain_PC[32] as Float

' Local storage of filter coefficients and history
Dim filter_coeffs[TOTAL_COEFFS] as Float
Dim filter_history[TOTAL_HISTORY] as Float


Function SOS(sos_input, my_index) as Float
  Dim out as Float
  Dim new_history as Float
  Dim idx as Long
  idx = my_index

  ' overall gain
  out = sos_input * filter_coeffs[idx*NUM_COEFFS+1]

  ' poles
  out = out - filter_history[idx*NUM_HISTORY+1] * filter_coeffs[idx*NUM_COEFFS+2]
  new_history = out - filter_history[idx*NUM_HISTORY+2] * filter_coeffs[idx*NUM_COEFFS+3]

  ' zeros
  out = new_history + filter_history[idx*NUM_HISTORY+1] * filter_coeffs[idx*NUM_COEFFS+4]
  out = out + filter_history[idx*NUM_HISTORY+2] * filter_coeffs[idx*NUM_COEFFS+5]

  filter_history[idx*NUM_HISTORY+2] = filter_history[idx*NUM_HISTORY+1]
  filter_history[idx*NUM_HISTORY+1] = new_history

  SOS = out
EndFunction

Function FilterModule(input_int, control, filter_index, aux) as Float
  ' handle a single filter module, where index is the index into the i/o buffers
  Dim filter_output as Float
  Dim sos_output as Float
  Dim sos_index as Long
  Dim input as Float
  ' Dim snap_value as Long
  input = input_int

  ' offset switch
  If((control And FCR_SW_OFFSET) > 0) Then
    input = input + offset_gain_PC[filter_index + 9]
  EndIf

  ' input switch
  If ((control And FCR_SW_INPUT) = 0) Then
    input = 0.0
  EndIf

  ' Calculate SOS
  For sos_index = 0 To (NUM_SOS-1)
    sos_output = SOS(input, sos_index + filter_index*NUM_SOS)
    If ((control And Shift_Left(FCR_SW_SOS1, sos_index)) > 0) Then
      input = sos_output
    Endif
  Next sos_index

  'multiplies input with set gain
  input = input * offset_gain_PC[filter_index + 1]

  'output switch
  If ((control And FCR_SW_OUTPUT) = 0) Then
    filter_output = 0.0
  Else
    filter_output = input
  EndIf

  ' ' Snap state that waits for aux signal
  ' If ((control And FCR_SW_SNAP) > 0) Then
  '   filter_output = 0.0
  '   snap_value = aux - (snap_config[filter_index+1] And snap_value_mask)
  '
  '   If ((snap_config[filter_index+1] And snap_lg) > 0) Then
  '     snap_value = -1 * snap_value
  '   EndIf
  '   If (snap_value <= 0) Then
  '     ' Disable snapping
  '     control = (control And Not(FCR_SW_SNAP))
  '     ' Enable output
  '     control = (control Or FCR_SW_INPUT)
  '     ' Stop ramp
  '     rcr = rcr And Not(0fh)
  '   EndIf
  ' EndIf

  if ((control And FCR_SW_AUX) > 0) Then
    filter_output = filter_output + aux - 8000h
  Endif

  FilterModule = filter_output
EndFunction

Sub RunFilters(input[], output[])
  Dim filter_index as Long
  Dim ii, jj, idx, rampidx as Long

  ' reload filter coefficients from PC if requested
  For ii = 0 To NUM_FILTERS-1
    ' also shift input level to +/- 32768
    input[ii + 1] = input[ii + 1] - 8000h
    If ((fbcr And Shift_Left(FBCR_RELOAD_1, ii)) > 0) Then
      idx = ii*NUM_SOS*NUM_COEFFS + 1
      ' Copy new coefficients for this filter from global field
      MemCpy(filter_coeffs_PC[idx], filter_coeffs[idx], NUM_SOS*NUM_COEFFS)

      ' Clear filter history
      ' TODO: is there a way to clear stuff, something like MemClear?
      idx = ii*NUM_SOS*NUM_HISTORY
      For jj = idx+1 To idx+(NUM_HISTORY*NUM_SOS)
        filter_history[jj] = 0.0
      Next jj

      ' Reset reload flag
      fbcr = fbcr And Not(Shift_Left(FBCR_RELOAD_1, ii))
    EndIf
  Next ii

  ' TODO: add option to sum DC onto the output, which would be an easy way to measure TFs
  ' TODO: there does not seem to be a way to calculate Par_[n], so need to hand-unroll this :(
  'For filter_index = 0 To (NUM_FILTERS-1)
  '  output[filter_index+1] = FilterModule(input[2*filter_index+1], Par_3, filter_index)
  'Next filter_index
  output[1] = FilterModule(input[1], fcr_1, 0, input[ 9]) + 8000h
  output[2] = FilterModule(input[2], fcr_2, 1, input[10]) + 8000h
  output[3] = FilterModule(input[3], fcr_3, 2, input[11]) + 8000h
  output[4] = FilterModule(input[4], fcr_4, 3, input[12]) + 8000h
  output[5] = FilterModule(input[5], fcr_5, 4, input[13]) + 8000h
  output[6] = FilterModule(input[6], fcr_6, 5, input[14]) + 8000h
  output[7] = FilterModule(input[7], fcr_7, 6, input[15]) + 8000h
  output[8] = FilterModule(input[8], fcr_8, 7, input[16]) + 8000h

EndSub
