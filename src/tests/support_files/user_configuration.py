# To use custom settings copy this sample to settings_local.py

#################### Local configuration ####################
# Use 0 to enable a dummy device. Otherwise use the channel number
DEVICE_NUM = 0  # The 0 is reserved to mock device for testing!
# LOG_LEVEL = 'INFO'  # Possible alternatives: DEBUG, INFO, WARNING, ERROR, CRITICAL
SETTINGS_FILE = "testing.json"

RAMP_FREQUENCY_MAX = 101
RAMP_FREQUENCY_MIN = 1
