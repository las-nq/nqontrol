from nqontrol import servo, general, ServoDevice, settings
from nqontrol.servo import FeedbackController
import unittest
from time import sleep, time
from websocket import create_connection
from ADwin import ADwinError
import multiprocessing as mp
from multiprocessing import Process
from math import pow
from pandas import DataFrame
import os
import logging as log
import nqontrol
from nqontrol.errors import *
from tempfile import TemporaryDirectory
from OpenQlab.analysis import ServoDesign
import numpy as np
from OpenQlab import io
log.basicConfig(format='%(levelname)s: %(module)s.%(funcName)s: %(message)s', level='INFO')


class TestConfiguration(unittest.TestCase):
    def test_run_develop_version(self):
        if 'site-packages' in nqontrol.__path__[0]:
            raise Exception('Not running the development code!!!')

    def test_user_config(self):
        RUNNING_IN_DOCKER = os.environ.get('RUNNING_IN_DOCKER', False)
        if RUNNING_IN_DOCKER:
            self.assertEqual(settings.DEVICES_LIST, [0])
            self.assertEqual(settings.SETTINGS_FILE, 'testing.json')


class TestBitManipulation(unittest.TestCase):
    def test_manipulate(self):
        x = 42
        y = general.changeBit(x, 2, True)
        self.assertEqual(y, 46)
        self.assertEqual(general.testBit(x, 2), 0)
        self.assertEqual(general.readBit(x, 2), False)
        self.assertEqual(general.testBit(y, 2), 1)
        self.assertEqual(general.readBit(y, 2), True)

        y = general.changeBit(y, 2, True)
        self.assertEqual(y, 46)
        self.assertEqual(general.testBit(y, 2), 1)
        self.assertEqual(general.readBit(y, 2), True)

        y = general.changeBit(y, 2, False)
        self.assertEqual(y, 42)
        self.assertEqual(general.testBit(y, 2), 0)
        self.assertEqual(general.readBit(y, 2), False)

        y = general.changeBit(y, 2, False)
        self.assertEqual(y, 42)
        self.assertEqual(general.testBit(y, 2), 0)
        self.assertEqual(general.readBit(y, 2), False)


class TestVoltConvertion(unittest.TestCase):
    def test_volt2int(self):
        self.assertIsInstance(servo._convertVolt2Int(3.4), int)
        self.assertIsInstance(servo._convertVolt2Int([3.4, 3]), np.ndarray)
        self.assertIsInstance(servo._convertVolt2Int([3.4, 3])[0], np.int64)

        mode = 0
        self.assertAlmostEqual(servo._convertVolt2Int(10, mode), 0x7fff)
        self.assertAlmostEqual(servo._convertVolt2Int(10, mode, True), 0xffff)
        self.assertAlmostEqual(servo._convertVolt2Int(3.7, mode), round(0x8000 * .37), 0)
        self.assertAlmostEqual(servo._convertVolt2Int(0, mode), 0)
        self.assertAlmostEqual(servo._convertVolt2Int(0, mode, True), 0x8000)
        self.assertAlmostEqual(servo._convertVolt2Int(-10, mode), -0x8000)
        self.assertAlmostEqual(servo._convertVolt2Int(-10, mode, True), 0)
        # out of range
        self.assertAlmostEqual(servo._convertVolt2Int(-20, mode, True), 0)
        self.assertAlmostEqual(servo._convertVolt2Int(20, mode, True), 0xffff)

        mode = 3
        self.assertAlmostEqual(servo._convertVolt2Int(1.25, mode), 0x7fff)
        self.assertAlmostEqual(servo._convertVolt2Int(0.4625, mode), round(0x8000 * .37), 0)
        self.assertAlmostEqual(servo._convertVolt2Int(0, mode), 0)
        self.assertAlmostEqual(servo._convertVolt2Int(-1.25, mode), -0x8000)
        # out of range
        self.assertAlmostEqual(servo._convertVolt2Int(5, mode, True), 0xffff)
        self.assertAlmostEqual(servo._convertVolt2Int(-5, mode, True), 0)

        # DataFrame

    def test_int2volt(self):
        mode = 0
        self.assertAlmostEqual(servo._convertInt2Volt(0x10000, mode), 10)
        self.assertAlmostEqual(servo._convertInt2Volt(-0x8000, mode), -10 - 10)
        self.assertAlmostEqual(servo._convertInt2Volt(0, mode), -10)
        self.assertAlmostEqual(servo._convertInt2Volt(0x8000 * .68, mode), 6.8 - 10)

        mode = 3
        self.assertAlmostEqual(servo._convertInt2Volt(0x8000, mode), 1.25 - 1.25)
        self.assertAlmostEqual(servo._convertInt2Volt(-0x8000, mode), -1.25 - 1.25)
        self.assertAlmostEqual(servo._convertInt2Volt(0, mode), 0 - 1.25)
        self.assertAlmostEqual(servo._convertInt2Volt(0x8000 * .68, mode), .85 - 1.25)

        # test with array
        self.assertListEqual(list(servo._convertInt2Volt([0, 0x8000, 0x8000 * 2])), [-10, 0, 10])


class TestServo(unittest.TestCase):
    def setUp(self):
        settings.NUMBER_OF_SERVOS = 8  # ensure this is 8 for all the servo tests, since sometimes numbers are explicitly set to 7, 8 etc..
        settings.NUMBER_OF_MONITORS = 8  # same thing
        self.sd = ServoDevice(settings.DEVICES_LIST[0], reboot=True)
        self.sd.reboot()
        self.testchannel = 2
        self.s = self.sd.servo(self.testchannel)
        import nqontrol
        log.warning('nqontrol path: {}'.format(nqontrol.__path__[0]))
        if 'site-packages' in nqontrol.__path__[0]:
            raise Exception('Not running the development code!!!')

    def test_checkNumberAndChannel(self):
        self.assertEqual(self.s._channel, self.testchannel)

    def test_filters(self):
        # default filter state
        filters = [
            [1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0]
        ]
        filtersEnabled = [False] * 5
        self.assertEqual(self.s._state['filters'], filters)
        self.assertEqual(self.s._state['filtersEnabled'], filtersEnabled)

        # change filters
        filters = [
            [1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0],
            [1, 0, 4, 0, 0],
            [1, 0, 0, 0, 0],
            [1, 0, 0, 8, 0]
        ]
        self.s.filters = filters
        self.assertEqual(self.s._state['filters'], filters)
        # Write junk to _filters to check reading
        self.s._filters = [[0.0] * 5] * 5
        self.assertEqual(self.s.filters, filters)
        self.assertEqual(self.s._state['filters'], filters)
        for i in range(3, settings.NUMBER_OF_SERVOS):
            self.assertEqual(self.sd.servo(i).filters, self.s.DEFAULT_FILTERS)

    def test_filterControlRegister(self):
        # change filtersEnabled
        filtersEnabled = [True] * 5
        self.assertEqual(self.s._state['filtersEnabled'], [False] * 5)
        self.s.filterStates = filtersEnabled
        self.assertEqual(self.s._state['filtersEnabled'], filtersEnabled)

        # disable one by one
        filtersEnabled[2] = False
        self.s.filterState(2, False)
        filtersEnabled[4] = False
        self.s.filterState(4, False)
        self.assertEqual(self.s._state['filtersEnabled'], filtersEnabled)

        # enable one by one
        filtersEnabled[2] = True
        self.s.filterState(2, True)
        filtersEnabled[4] = True
        self.s.filterState(4, True)
        self.assertEqual(self.s._state['filtersEnabled'], filtersEnabled)

        # change the other bool states
        self.assertEqual(self.s._state['auxSw'], False)
        self.assertEqual(self.s._state['offsetSw'], False)
        self.assertEqual(self.s._state['outputSw'], False)
        self.assertEqual(self.s._state['inputSw'], False)
        self.s.auxSw = True
        self.s.offsetSw = True
        self.s.outputSw = True
        self.s.inputSw = True
        # read the states from ADwin
        self.s._readFilterControl()
        self.assertEqual(self.s._state['auxSw'], True)
        self.assertEqual(self.s._state['offsetSw'], True)
        self.assertEqual(self.s._state['outputSw'], True)
        self.assertEqual(self.s._state['inputSw'], True)

    def test_fifoOutput(self):
        # enable FIFO and test correct Parameters
        stepsize = 100
        self.s.enableFifo(stepsize)
        self.assertEqual(2, self.s._adw.Get_Par(settings.PAR_ACTIVE_CHANNEL))  # Test channel
        self.assertEqual(stepsize, self.s._adw.Get_Par(settings.PAR_FIFOSTEPSIZE))  # Test stepsize
        self.s.disableFifo()
        self.assertFalse(self.s.fifoEnabled)
        self.s.enableFifo(1)
        self.assertEqual(self.s.fifoStepsize, 1)
        with self.assertRaises(UserInputError):
            self.s.fifoStepsize = 0

    def test_monitor(self):
        # assign different channels
        self.sd.enableMonitor(8, self.testchannel, card='input')
        self.sd.enableMonitor(5, self.testchannel, card='aux')
        self.sd.enableMonitor(1, self.testchannel, card='output')
        self.sd.enableMonitor(2, self.testchannel, card='input')
        self.sd.enableMonitor(3, self.testchannel, card='ttl')
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 8, 1)[0], 2)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 5, 1)[0], 10)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 1, 1)[0], 22)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 2, 1)[0], 2)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 3, 1)[0], 30)

        # disable monitor
        self.sd.disableMonitor(1)
        self.sd.disableMonitor(8)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 1, 1)[0], 0)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_MONITORS, 8, 1)[0], 0)

        # try wrong channels
        with self.assertRaises(IndexError):
            self.sd.enableMonitor(0, self.testchannel, card='input')
        with self.assertRaises(IndexError):
            self.sd.enableMonitor(9, self.testchannel, card='input')
        with self.assertRaises(IndexError):
            self.sd.enableMonitor(-1, self.testchannel, card='input')

    def test_saveLoadJson(self):
        # TODO better test
        self.s.fifoStepsize = 3
        self.s.enableRamp(30, .1)
        self.s.realtimePlot()
        sleep(.1)
        self.s.stopRealtimePlot()
        self.s.servoDesign.integrator(100)
        self.s.servoDesign.notch(500, Q=4)
        servoDesign_str = self.s.servoDesign.__str__()
        self.s.gain = 20345.190
        self.s.saveJsonToFile('servo.json')
        self.s.gain = 10
        self.assertEqual(self.s._state['gain'], 10)
        self.s.loadSettings('servo.json')
        self.assertEqual(self.s._state['gain'], 20345.190)
        self.assertEqual(self.s.servoDesign.__str__(), servoDesign_str)
        self.sd.removeServo(3)
        self.sd.addServo(3, 'servo.json')
        self.assertEqual(self.sd.servo(3)._state['gain'], 20345.190)
        self.assertEqual(self.sd.servo(3)._channel, 3)
        self.assertEqual(type(self.sd.servo(3)._ramp), mp.managers.DictProxy)

    def test_offsetGain(self):
        self.s.offset = 9.4
        self.s.gain = 1.5
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel, 1)[:], [1.5])
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel + 8, 1)[0], 30802)

        self.s.gain = 1.0
        self.s.inputSensitivity = 3
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel, 1)[:], [0.125])
        self.s.offset = 1
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel + 8, 1)[:], [26214])

    def test_offsetLimits(self):
        for mode in range(4):
            limit = round(10 / pow(2, mode), 2)
            self.s.inputSensitivity = mode
            self.s.offset = limit
            self.assertEqual(self.s.offset, limit)
            self.s.offset = limit + .1
            self.assertEqual(self.s.offset, limit)
            # set offset before sensitivity
            self.s.inputSensitivity = 0
            self.s.offset = 20
            self.s.inputSensitivity = mode
            self.assertEqual(self.s.offset, limit)

    def test_ramp(self):
        settings.SAMPLING_RATE = settings.RAMP_DATA_POINTS

        # what happens if stepsize is still None
        startfreq = self.s.rampFrequency

        self.s.enableRamp(5, 5)
        self.assertEqual(self.s._ramp['amplitude'], 5)
        self.assertEqual(self.s._adw.Get_Par(settings.PAR_RCR), 1282)
        self.assertEqual(self.s._adw.Get_FPar(settings.FPAR_RAMPAMP), 0.5)
        self.s.realtimePlot()
        settings.SAMPLING_RATE = 200e3
        sleep(.1)
        self.s.stopRealtimePlot()

        settings.SAMPLING_RATE = settings.RAMP_DATA_POINTS
        self.s.enableRamp(255, 10)
        self.assertEqual(self.s._ramp['amplitude'], 10)
        self.assertTrue(self.s.rampEnabled)
        self.assertEqual(self.s._adw.Get_Par(settings.PAR_RCR), 65282)
        self.assertEqual(self.s._adw.Get_FPar(settings.FPAR_RAMPAMP), 1.0)
        settings.SAMPLING_RATE = 200e3
        self.s.realtimePlot()
        sleep(.1)
        self.s.stopRealtimePlot()

        self.sd.removeServo(5)
        self.assertIsNone(self.sd._servos[4])
        self.s.disableRamp()
        for s in self.sd._servos:
            if s is not None:
                self.assertFalse(s.rampEnabled)

        self.s.enableRamp(1, 2)
        self.assertTrue(self.s.fifoEnabled)
        self.assertEqual(self.s._fifo['maxlen'], self.s._fifo['maxlen'])
        self.s.disableRamp()

        # function accepts empty parameter list
        self.s.enableRamp()

    def test_inputGain(self):
        self.assertEqual(self.s._adw.Get_Par(settings.PAR_SENSITIVITY), 0)

        self.s.inputSensitivity = 2
        self.sd.servo(8).inputSensitivity = 3
        self.assertEqual(self.s._adw.Get_Par(settings.PAR_SENSITIVITY), 49160)

        self.s.auxSensitivity = 3
        self.assertEqual(self.s._adw.Get_Par(settings.PAR_SENSITIVITY), 835592)
        self.sd.servo(7).auxSensitivity = 2
        self.assertEqual(self.s._adw.Get_Par(settings.PAR_SENSITIVITY), 537706504)

    def test_runtime(self):
        self.s.enableFifo(1)
        self.s.enableRamp(1, 1)
        for i in range(1, 9):
            self.sd.enableMonitor(i, i, card='input')
            self.sd.servo(i).offset = 3
            self.sd.servo(i).auxSw = True
            self.sd.servo(i).filters = [[7] * 5] * 5
        sleep(.1)
        self.assertLess(self.s._adw.Get_Par(settings.PAR_TIMEDIFF), 4500)
        self.assertGreater(self.s._adw.Get_Par(settings.PAR_TIMEDIFF), 2500)

    def test_sampling_rate(self):
        CPU_CLK = 1e9
        self.assertAlmostEqual(self.s._adw.Get_Processdelay(1), CPU_CLK / settings.SAMPLING_RATE)

    def test_applyServoDesign(self):
        sDesign = ServoDesign()
        sDesign.integrator(1e3)
        sDesign.notch(2e4, 1)
        sDesign.filters[1].enabled = False
        sDesign.lowpass(1e5)
        settings.SAMPLING_RATE = 100000
        self.s.applyServoDesign(sDesign)
        filters = [
            [1.0313938638494844, -0.9999371701207665, 0.0, -0.9390625058174923, 0.0],
            [0.6777233810861951, -0.41885608448176614, 0.35544676217239035, -0.6180339887498948, 1.0],
            [9.869604401089363, -2.0000000000000004, 1.0000000000000004, 2.0, 1.0],
            [1.0, 0, 0, 0, 0],
            [1.0, 0, 0, 0, 0]
        ]
        self.assertEqual(self.s.filters, filters)
        self.assertEqual(self.s.filterStates, [True, False, True, False, False])

    def test_calculateRefreshTime(self):
        self.s._fifo['stepsize'] = settings.SAMPLING_RATE
        self.s._fifo['maxlen'] = 1000
        self.s._calculateRefreshTime()
        self.assertAlmostEqual(self.s.realtime['refreshTime'], 500)

        self.s._fifo['stepsize'] = 1
        self.s._calculateRefreshTime()
        self.assertAlmostEqual(self.s.realtime['refreshTime'], self.s.MIN_REFRESH_TIME)

    def test_readoutNewData(self):
        self.s.fifoStepsize = 1
        self.s._waitForBufferFilling()
        data = self.s._readoutNewData(self.s._fifo['maxlen'])
        self.assertEqual(type(data), DataFrame)
        self.assertEqual(len(data), self.s._fifo['maxlen'])
        # Readout 0 data
        data = self.s._readoutNewData(0)
        self.assertEqual(type(data), DataFrame)
        self.assertEqual(len(data), 0)
        self.assertListEqual(list(data.columns), self.s.realtime['ydata'])

    def test_prepareContinuousData(self):
        self.s.fifoStepsize = int(settings.SAMPLING_RATE / 100)
        for i in range(10):
            self.s._prepareContinuousData()
            sleep(.01)

        self.s.fifoStepsize = 1
        self.s._waitForBufferFilling()
        self.s._prepareContinuousData()
        dc = self.s._fifoBuffer
        self.assertEqual(len(dc), self.s._fifo['maxlen'])
        self.s._waitForBufferFilling()
        dc = self.s._prepareData()
        self.assertTrue(dc.equals(self.s._fifoBuffer))
        self.assertEqual(len(dc), self.s._fifo['maxlen'])
        self.s.fifoStepsize = int(settings.SAMPLING_RATE)
        i = 0
        while self.s._fifoBufferSize > 10:
            self.s._prepareContinuousData()
            i += 1
            if i > 200:
                assert False, "Can't read fast enough."

    def test_timeForFifoCycles(self):
        self.s._fifo['stepsize'] = 1
        self.assertEqual(self.s._timeForFifoCycles(10), 10 / settings.SAMPLING_RATE)

    def test_waitForBufferFilling(self):
        self.s.fifoStepsize = 10
        t1 = time()
        self.s._waitForBufferFilling()
        t2 = time()
        t = 10 / settings.SAMPLING_RATE * self.s._fifo['maxlen']
        self.assertAlmostEqual(t2 - t1, t, places=2)

    def test_prepareRampData(self):
        for n in (100, 255, 1, 13):
            log.info('n = {}'.format(n))
            self.s.enableRamp(n, 10 / n)
            self.s._prepareRampData()
            self.s._prepareRampData()  # 2 times that it can fail one time
            dc = self.s._fifoBuffer
            self.assertEqual(len(dc), self.s._fifo['maxlen'])
            dc = self.s._prepareData()
            self.assertEqual(len(dc), self.s._fifo['maxlen'])

    def test_realtimePlot(self):
        plot_sleep_time = .1
        self.s.fifoStepsize = 10
        self.s.realtimePlot(refreshTime=.06)
        self.s.realtime['ylim'] = (-.1, .2)
        sleep(plot_sleep_time)
        self.s.stopRealtimePlot()

        self.s.enableRamp(10, .1)
        self.s.realtimePlot()
        sleep(plot_sleep_time)
        self.s.realtime['ydata'] = ['aux', 'output']
        self.s.enableRamp(5, .05)
        sleep(plot_sleep_time)
        self.s.disableRamp()
        sleep(.1)
        self.s.stopRealtimePlot()

    def test_tooManyRealtimePlots(self):
        self.s.realtimePlot()
        sleep(.1)
        with self.assertRaises(UserInputError):
            self.s.realtimePlot()
        self.s.stopRealtimePlot()

    def test_fastRamp(self):
        plot_sleep_time = .1
        self.s.enableRamp(255, .1)
        self.s.realtimePlot()
        sleep(plot_sleep_time)
        self.s.stopRealtimePlot()
        self.s.disableRamp()

    def workload_loop(self, N):
        for n in range(10):
            self.sd.workload
            self.sd.timestamp
            self.s.filters = [[1] * 5] * 5

    def test_break_adwin_with_multiprocessing(self):
        processes = []
        for n in range(10):
            p = Process(target=self.workload_loop, args=(n,))
            processes.append(p)
        for p in processes:
            p.start()
        for p in processes:
            p.join()

    def test_check_input_offset(self):
        self.s = self.sd.servo(8)
        self.s.fifoStepsize = 1
        self.s._waitForBufferFilling()
        data = self.s._readoutNewData(self.s._fifo['maxlen'])
        self.assertAlmostEqual(data['aux'].mean(), 0, places=1)
        self.assertAlmostEqual(data['input'].mean(), 0, places=1)

    def test_convertFrequency2Stepsize(self):
        self.assertEqual(servo._convertFrequency2Stepsize(.001), 1)
        self.assertEqual(servo._convertFrequency2Stepsize(1e9), 255)
        self.assertIsNone(servo._convertFrequency2Stepsize(None))
        settings.SAMPLING_RATE = 200e3
        self.assertEqual(servo._convertFrequency2Stepsize(127), 83)

    def test_convertStepsize2Frequency(self):
        self.assertIsNone(servo._convertStepsize2Frequency(None))

    def test_rampAmplitude(self):
        self.s.rampAmplitude = 5.6
        self.assertEqual(self.s.rampAmplitude, 5.6)
        with self.assertRaises(UserInputError):
            self.s.rampAmplitude = 10.1
        with self.assertRaises(UserInputError):
            self.s.rampAmplitude = -0.1

    def test_rampFrequency(self):
        self.s.rampFrequency = 47.5
        self.assertAlmostEqual(self.s.rampFrequency, 47.5, places=0)

    def test_fifo_switching_with_ramp_bug(self):
        s1 = self.sd.servo(3)
        s2 = self.sd.servo(4)
        s1.enableRamp(50, 5)
        s1._prepareData()
        s2.enableFifo()
        s2._waitForBufferFilling()
        s2._prepareData()
        s1._prepareData()

    def test_send_state_after_reboot(self):
        sleep(.1)
        time = self.sd.timestamp
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel, 1)[:], [1.0])
        self.s.gain = 1.5
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel, 1)[:], [1.5])
        self.sd.reboot()
        self.assertEqual(self.s._adw.GetData_Double(settings.DATA_OFFSETGAIN, self.testchannel, 1)[:], [1.5])
        self.assertLessEqual(self.sd.timestamp, time)

    def test_saving_and_loading_with_plant_and_temp_feedback(self):
        tests_dir = os.path.dirname(os.path.abspath(__file__))
        self.s.servoDesign.integrator(400)
        dc = io.read('{}/fra.csv'.format(tests_dir))
        self.s.servoDesign.plant = dc
        self.s.tempFeedbackStart(.84, (1, 3), voltage_limit=3.6, update_interval=1.3)
        servoDesign_str = self.s.servoDesign.__str__()
        plant = self.s.servoDesign.plant.copy()
        self.s.saveJsonToFile('servo.json')
        self.s.tempFeedbackStop()
        self.assertIsNone(self.s.tempFeedback)
        self.s._tempFeedbackSettings = {}
        self.assertEqual(len(plant), len(self.s.servoDesign.plant))
        self.s.servoDesign = None
        self.s.loadSettings('servo.json')
        self.assertEqual(servoDesign_str, self.s.servoDesign.__str__())
        self.assertEqual(self.s._tempFeedbackSettings['dT'], .84)
        self.assertEqual(tuple(self.s._tempFeedbackSettings['mtd']), (1, 3))
        self.assertEqual(self.s._tempFeedbackSettings['voltage_limit'], 3.6)
        self.assertEqual(self.s._tempFeedbackSettings['update_interval'], 1.3)

    def test_apply_old_settings(self):
        self.s.outputSw = True
        dict = self.s.getSettingsDict()
        # del dict['_state']['snapSw']
        # self.s.snapSw = True
        self.s.outputSw = False
        self.assertFalse(self.s.outputSw)
        self.s.loadSettings(dict)
        # self.assertTrue(self.s.snapSw)
        self.assertTrue(self.s.outputSw)

    def test_offset_autoset(self):
        self.s.inputSensitivity = 3
        self.s.offsetAutoSet()
        self.s.disableFifo()
        self.s.gain = 10
        self.assertAlmostEqual(self.s.offset, 0, places=1)
        self.assertNotEqual(self.s.offset, 0)
        log.warning('offset: {}'.format(self.s.offset))
        sleep(.1)

        self.s.inputSw = True
        self.s.outputSw = True
        self.s.filterState(1, True)
        self.s.enableFifo()
        self.s._waitForBufferFilling()
        df = self.s._readoutNewData(10000)['output']
        mean_uncorrected = df.mean()
        log.warning(str(df.head()))
        log.warning(f'length of data: {len(df)}')
        self.assertNotEqual(mean_uncorrected, 0)
        self.s.offsetSw = True
        self.s._waitForBufferFilling()
        mean_corrected = self.s._readoutNewData(10000)['output'].mean()
        self.assertNotEqual(mean_corrected, 0)
        self.assertAlmostEqual(mean_corrected, 0, places=1)

    def test_check_control_switch_updates(self):
        self.assertFalse(self.s.inputSw)
        self.assertFalse(self.s.outputSw)
        self.assertFalse(self.s.offsetSw)
        # self.assertFalse(self.s.snapSw)
        self.assertFalse(self.s.auxSw)
        self.s.inputSw = True
        self.s.outputSw = True
        self.s.offsetSw = True
        # self.s.snapSw = True
        self.s.auxSw = True
        self.s._state['inputSw'] = False
        self.assertTrue(self.s.inputSw)
        self.s._state['outputSw'] = False
        self.assertTrue(self.s.outputSw)
        self.s._state['offsetSw'] = False
        self.assertTrue(self.s.offsetSw)
        # self.s._state['snapSw'] = False
        # self.assertTrue(self.s.snapSw)
        self.s._state['auxSw'] = False
        self.assertTrue(self.s.auxSw)

    def test_lock_getters_and_setters(self):
        with self.assertRaises(TypeError):
            self.s.lockThreshold = 'wrong type'
        with self.assertRaises(TypeError):
            self.s.lockSearchMin = 'wrong type'
        with self.assertRaises(TypeError):
            self.s.lockSearchMax = 'wrong type'
        with self.assertRaises(ValueError):
            self.s.lockSearchMin = -12
        with self.assertRaises(ValueError):
            self.s.lockSearchMax = 13
        self.s.lockSearchMax = 3
        with self.assertRaises(ValueError):
            self.s.lockSearchMin = 5
        self.s.lockSearchMin = 2
        with self.assertRaises(ValueError):
            self.s.lockSearchMax = 0
        with self.assertRaises(TypeError):
            self.s.lockGreater = 345
        with self.assertRaises(TypeError):
            self.s.lockGreater = 'hello'
        with self.assertRaises(TypeError):
            self.s.relock = 345
        with self.assertRaises(TypeError):
            self.s.relock = 'hello'

    def test_servo_autolock(self):
        with self.assertRaises(TypeError):
            self.s.autolock('tach')
        with self.assertRaises(ValueError):
            self.s.autolock(5)
        with self.assertRaises(TypeError):
            self.s.autolock(1, greater='bla')
        with self.assertRaises(TypeError):
            self.s.autolock(0, threshold='bla')
        with self.assertRaises(TypeError):
            self.s.autolock(0, min='bla')
        with self.assertRaises(TypeError):
            self.s.autolock(0, max='bla')

        indexoffset = (self.s._channel - 1) * 5
        print(indexoffset)
        self.s.autolock(1, 9, 0, 5, True, True)  # setting threshold to 9 should make sure it never runs out of lock state 1
        threshold = servo._convertVolt2Int(9, self.s.auxSensitivity, True)
        threshold = general.changeBit(threshold, 16, True)
        min = servo._convertVolt2Int(0, self.s.auxSensitivity, True)
        max = servo._convertVolt2Int(5, self.s.auxSensitivity, True)
        lock = general.changeBit(1, 2, True)
        # testing the lock parameters
        sleep(.1)
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_LOCK, 1 + indexoffset, 1)[:], [lock])  # will this work?
        self.assertEqual(self.s._adw.GetData_Long(settings.DATA_LOCK, 2 + indexoffset, 3)[:], [threshold, min, max])

    def test_lock_search_state(self):
        # set autolock to value that cant be reached, so lock continues looping

        # lock greater
        self.s.autolock(1, 8, -1, 1, True, False)
        # switches need to be off for output and Input
        sleep(.1)
        self.assertFalse(self.s.inputSw)
        self.assertFalse(self.s.outputSw)
        # check lock state, should be `1`
        indexoffset = (self.s._channel - 1) * 5
        lock = self.s._adw.GetData_Long(settings.DATA_LOCK, 1 + indexoffset, 1)[0]
        lock = (lock & 0x3)
        self.assertEqual(lock, 1)

        # lock smaller
        self.s.autolock(1, -9, -1, 1, False, True)
        sleep(.1)
        self.assertFalse(self.s.inputSw)
        self.assertFalse(self.s.outputSw)
        # check lock state, should be `1`
        lock = self.s._adw.GetData_Long(settings.DATA_LOCK, 1 + indexoffset, 1)[0]
        lock = (lock & 0x3)
        self.assertEqual(lock, 1)

    def test_lock_found_state(self):
        # set autolock to value that will always be reached, so lock gets activated

        # lock greater
        self.s.autolock(1, -9, -1, 1, True, False)
        sleep(.4)
        # state should be `2`
        # input/output should be enabled
        self.assertTrue(self.s.inputSw)
        self.assertTrue(self.s.outputSw)
        indexoffset = (self.s._channel - 1) * 5
        lock = self.s._adw.GetData_Long(settings.DATA_LOCK, 1 + indexoffset, 1)[0]
        lock = (lock & 0x3)
        self.assertEqual(lock, 2)

        # lock smaller
        self.s.autolock(1, 9, -1, 1, False, False)
        sleep(.3)
        self.assertTrue(self.s.inputSw)
        self.assertTrue(self.s.outputSw)
        # check lock state, should be `2`
        lock = self.s._adw.GetData_Long(settings.DATA_LOCK, 1 + indexoffset, 1)[0]
        lock = (lock & 0x3)
        self.assertEqual(lock, 2)

    def test_lock_off_state(self):
        self.s.inputSw = True
        self.s.outputSw = True
        self.s.autolock(0)
        sleep(.1)
        self.assertTrue(self.s.inputSw)
        self.assertTrue(self.s.outputSw)
        # check lock state
        indexoffset = (self.s._channel - 1) * 5
        lock = self.s._adw.GetData_Long(settings.DATA_LOCK, 1 + indexoffset, 1)[0]
        lock = (lock & 0x3)
        self.assertEqual(lock, 0)

    def test_lock_disables_ramp(self):
        self.s.enableRamp()
        self.s.autolock(1)
        sleep(1e-3)
        self.assertFalse(self.s.rampEnabled)

    def test_lock_disables_output_input(self):
        def out():
            return self.s._readoutNewData(10000)['output'].iloc[-1]

        self.s.enableFifo()
        self.assertEqual(out(), 0)
        self.s.offset = 5
        self.s.offsetSw = True
        self.s.inputSw = True
        self.s.outputSw = True
        sleep(.01)
        self.assertGreater(out(), 2)
        self.s.autolock(1, 9, 1, 2, True, False)
        sleep(.01)
        self.assertFalse(self.s.inputSw)
        self.assertFalse(self.s.outputSw)
        self.assertLessEqual(out(), 2)
        self.assertGreaterEqual(out(), 1)


class TestServoDevice(unittest.TestCase):
    def setUp(self):
        self.sd = ServoDevice(settings.DEVICES_LIST[0])

    def _test_addServo(self):
        with self.assertRaises(IndexError):
            self.sd.addServo(0)
        with self.assertRaises(IndexError):
            self.sd.addServo(9)
        self.sd.addServo(4)
        self.assertIsInstance(self.sd._servos[3], servo.Servo)

    def test_saveJson(self):
        settings.CREATE_SETTINGS_BACKUP = False
        self.sd.servo(3).inputSensitivity = 1
        self.sd.servo(3).offset = 3.75
        self.sd.servo(8).offset = 8.8882
        self.sd.enableMonitor(3, 3, 'output')
        monitors = self.sd.monitors.copy()
        self.sd.saveDeviceToJson('servos.json')
        self.sd.servo(3).offset = 1.2
        self.sd.servo(8).offset = 4.9
        self.sd.enableMonitor(3, 3, 'input')
        self.assertEqual(self.sd.servo(3)._state['offset'], 1.2)
        self.sd.loadDeviceFromJson('servos.json')
        self.assertEqual(self.sd.servo(3)._state['offset'], 3.75)
        self.assertEqual(self.sd.servo(3).inputSensitivity, 1)
        self.assertEqual(self.sd.servo(8)._state['offset'], 8.8882)
        self.assertEqual(self.sd.monitors[3 - 1]['card'], 'output')
        self.assertEqual(self.sd.monitors, monitors)

        self.sd.deviceNumber = 94
        self.sd.saveDeviceToJson('servos.json')
        sd2 = ServoDevice(settings.DEVICES_LIST[0], readFromFile='servos.json')
        self.assertEqual(sd2.deviceNumber, settings.DEVICES_LIST[0])

        self.assertEqual(sd2.servo(8)._state['offset'], self.sd.servo(8)._state['offset'])

    def test_saving_and_loading_with_plant(self):
        settings.CREATE_SETTINGS_BACKUP = False
        tests_dir = os.path.dirname(os.path.abspath(__file__))
        self.sd.servoDesign.integrator(400)
        dc = io.read('{}/fra.csv'.format(tests_dir))
        self.sd.servoDesign.plant = dc
        servoDesign_str = self.sd.servoDesign.__str__()
        plant = self.sd.servoDesign.plant.copy()
        self.sd.saveDeviceToJson('sd.json')
        self.assertEqual(len(plant), len(self.sd.servoDesign.plant))
        self.sd._servoDesign = None
        self.sd.loadDeviceFromJson('sd.json')
        self.assertEqual(servoDesign_str, self.sd.servoDesign.__str__())

    def test_backupSettingsFile(self):
        with TemporaryDirectory() as tmpdir:
            savefile = "{}/sd.json".format(tmpdir)
            self.assertEqual(len(os.listdir(tmpdir)), 0)
            settings.CREATE_SETTINGS_BACKUP = False
            # First saving
            self.sd.saveDeviceToJson(savefile)
            # manual backup
            self.sd._backupSettingsFile(savefile)
            self.assertEqual(len(os.listdir(tmpdir)), 2)
            # Backup should not be created
            self.sd.saveDeviceToJson(savefile)
            self.assertEqual(len(os.listdir(tmpdir)), 2)
            # Now it should be created
            settings.CREATE_SETTINGS_BACKUP = True
            assert settings.CREATE_SETTINGS_BACKUP
            sleep(1)
            self.sd.saveDeviceToJson(savefile)
            self.assertEqual(len(os.listdir(tmpdir)), 3)

            # Test it will not overwrite an existing backup file
            settings.BACKUP_SUBSTRING = 'will_fail'
            self.sd.saveDeviceToJson(savefile)
            with self.assertRaises((IOError, OSError)):
                self.sd.saveDeviceToJson(savefile)

    def test_servo_names(self):
        settings.DEVICES_LIST = [0]
        settings.NUMBER_OF_SERVOS = 3
        settings.SERVO_NAMES[0] = {
            1: 'Cavity',
            2: 'Testing',
            3: 'Mode Cleaner',
        }
        sd = ServoDevice(0)
        self.assertEqual(sd.servo(1).name, 'Cavity')
        self.assertEqual(sd.servo(3).name, 'Mode Cleaner')


class TestTemperatureFeedback(unittest.TestCase):
    def setUp(self):
        self.sd = ServoDevice(settings.DEVICES_LIST[0])
        self.sd.reboot()
        self.s = self.sd.servo(2)
        self.fc = FeedbackController(self.s, 1, (1, 2), 4, settings.DEFAULT_TEMP_HOST, settings.DEFAULT_TEMP_PORT)

    def test_tempFeedbackConnection(self):
        message = 'mtd:1,2:3'

        s = create_connection(f'ws://127.0.0.1:5917')
        s.send(message)
        answer = s.recv()
        s.close()

        self.assertIn('OK', answer)
        self.assertIn('23.0°C', answer)

    def test_sendMessage(self):
        answer = self.fc._send(3)
        self.assertIn('OK.', answer)

    def test_calculateFeedback(self):
        dT = self.fc._calculateFeedback(10)
        self.assertAlmostEqual(dT, 1)

    def test_conditions(self):
        self.assertTrue(self.fc._checkConditions())
        self.s.enableRamp(1, 1)
        self.assertFalse(self.fc._checkConditions())

    def test_last_output(self):
        self.assertEqual(self.fc._last_output(), 0)
        self.s.enableRamp(50, 10)
        self.assertNotEqual(self.fc._last_output(), 0)
        out = []
        for _ in range(100):
            out.append(self.fc._last_output())
        print(out)
        self.assertGreater(max(out), 5)
        self.assertLess(min(out), -5)

    def test_threading(self):
        self.s.tempFeedbackStart(10, (1, 2), 0)
        sleep(.2)
        self.assertIn('OK.', self.s._tempFeedback.last_answer)
        self.s.tempFeedbackStop()

    def test_adding_plant_data_string(self):
        tests_dir = os.path.dirname(os.path.abspath(__file__))
        testfile = '{}/fra.csv'.format(tests_dir)
        compare = io.read(testfile)

        #file is encoded in 'cp1252'
        with open(testfile, mode='rb') as file:
            data_string = file.read().decode('cp1252')

        data = io.read(data_string)

        self.assertTrue(data.equals(compare))


if __name__ == '__main__':
    # t = TestTemperatureFeedback()
    # t.setUp()
    # t.test_tempFeedbackConnection()
    unittest.main()
