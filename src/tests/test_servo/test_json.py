# pylint: disable=protected-access
import logging as log
import os
import unittest
from tempfile import NamedTemporaryFile
from time import sleep

from openqlab import io

from nqontrol import Servo, ServoDevice
from nqontrol.general import settings
from nqontrol.general.mockAdwin import MockADwin

from .test_servo import TestServoTemplate

log.basicConfig(
    format="%(levelname)s: %(module)s.%(funcName)s: %(message)s", level="INFO"
)

settings.NUMBER_OF_SERVOS = 8
settings.NUMBER_OF_MONITORS = 8


class TestServoJson(TestServoTemplate):
    def setUp(self):
        super().setUp()
        self.test_file = NamedTemporaryFile()

    def tearDown(self):
        super().tearDown()
        self.test_file.close()

    def test_saveLoadJson(self):
        # TODO better test
        self.s.enableFifo(3)
        self.s.enableRamp()
        self.s.realtimePlot()
        sleep(0.1)
        self.s.stopRealtimePlot()
        self.s.servoDesign.integrator(100)
        self.s.servoDesign.notch(500, 4)
        servoDesign_str = self.s.servoDesign.__str__()
        self.s.gain = 20345.190
        self.s.saveJsonToFile(self.test_file.name)
        self.s.gain = 10
        self.assertEqual(self.s._state["gain"], 10)
        self.s.loadSettings(self.test_file.name)
        self.assertEqual(self.s._state["gain"], 20345.190)
        self.assertEqual(self.s.servoDesign.__str__(), servoDesign_str)
        self.sd.removeServo(3)
        self.sd.addServo(3, self.test_file.name)
        self.assertEqual(self.sd.servo(3)._state["gain"], 20345.190)
        self.assertEqual(self.sd.servo(3)._channel, 3)

    def test_load_settings(self):
        with self.assertRaises(TypeError):
            self.s.loadSettings(1)

    def test_read_json_from_file(self):
        with self.assertRaises(FileNotFoundError):
            self.s._readJsonFromFile("I hate sand")

    def test_saving_and_loading_with_plant_and_temp_feedback(self):
        tests_dir = os.path.dirname(os.path.abspath(__file__))
        self.s.servoDesign.integrator(400)
        dc = io.read(f"{tests_dir}/../support_files/fra.csv")
        self.s.servoDesign.plant = dc
        self.s.tempFeedbackStart(0.84, (1, 3), voltage_limit=3.6, update_interval=1.3)
        servoDesign_str = self.s.servoDesign.__str__()
        plant = self.s.servoDesign.plant.copy()
        self.s.saveJsonToFile(self.test_file.name)
        self.s.tempFeedbackStop()
        self.assertIsNone(self.s.tempFeedback)
        self.s._tempFeedbackSettings = {}
        self.assertEqual(len(plant), len(self.s.servoDesign.plant))
        self.s.servoDesign = None
        self.s.loadSettings(self.test_file.name)
        self.assertEqual(servoDesign_str, self.s.servoDesign.__str__())
        self.assertEqual(self.s._tempFeedbackSettings["dT"], 0.84)
        self.assertEqual(tuple(self.s._tempFeedbackSettings["mtd"]), (1, 3))
        self.assertEqual(self.s._tempFeedbackSettings["voltage_limit"], 3.6)
        self.assertEqual(self.s._tempFeedbackSettings["update_interval"], 1.3)

    def test_apply_old_settings(self):
        self.s.outputSw = True
        dic = self.s.getSettingsDict()
        # del dict['_state']['snapSw']
        # self.s.snapSw = True
        self.s.outputSw = False
        self.assertTrue(dic["_state"]["outputSw"])
        self.assertFalse(self.s.outputSw)
        self.s.loadSettings(dic)
        # self.assertTrue(self.s.snapSw)
        self.assertTrue(self.s.outputSw)

    def test_read_settings_from_adwin_before_saving(self):
        sd2 = ServoDevice(settings.DEVICE_NUM)
        s = sd2.servo(2)
        s._adw = self.s._adw

        s.inputSw = True
        s.offset = 0.84
        s.gain = 94.1
        s.inputSensitivity = 3
        s.auxSensitivity = 2
        s.filterStates = [True, False, True, True, False]

        # pre-check if mockAdwin is the same
        self.assertEqual(s._adw, self.s._adw)

        if isinstance(s._adw, MockADwin):
            self.assertListEqual(s._adw._par, self.s._adw._par)

        self.s.filters = [
            [2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6],
        ]
        new_filters = [
            [20, 30, 40, 50, 60],
            [20, 30, 40, 50, 60],
            [20, 30, 40, 50, 60],
            [20, 30, 40, 50, 60],
            [20, 30, 40, 50, 60],
        ]
        s.filters = new_filters

        dic = self.s.getSettingsDict()

        self.assertEqual(dic["_state"]["inputSw"], True)
        self.assertAlmostEqual(dic["_state"]["offset"], 0.84)
        self.assertAlmostEqual(dic["_state"]["gain"], 94.1)
        self.assertEqual(dic["_state"]["inputSensitivity"], 3)
        self.assertEqual(dic["_state"]["auxSensitivity"], 2)
        self.assertEqual(
            list(dic["_state"]["filtersEnabled"]), [True, False, True, True, False]
        )
        self.assertEqual(list(dic["_state"]["filters"]), new_filters)

    def test_saving_of_autolock_parameters(self):
        self.s.lockStepsize = 0.001
        self.s.lockGreater = True
        self.s.lockAmplitude = 4.7
        self.s.lockOffset = -1.8

        dic = self.s.getSettingsDict()

        self.assertEqual(dic["_autolock"]["stepsize"], 0.001)
        self.assertEqual(dic["_autolock"]["greater"], 1)
        self.assertEqual(dic["_autolock"]["amplitude"], 4.7)
        self.assertEqual(dic["_autolock"]["offset"], -1.8)

    def test_apply_autolock_parameters(self):
        dic = self.s.getSettingsDict()

        dic["_autolock"]["stepsize"] = 0.00021
        dic["_autolock"]["greater"] = 0
        dic["_autolock"]["amplitude"] = 1.4
        dic["_autolock"]["offset"] = 2.8
        dic["_autolock"]["threshold"] = 3.4
        dic["_autolock"]["threshold_break"] = 3.1
        dic["_autolock"]["relock"] = 1

        s = Servo(3, MockADwin(), applySettings=dic)

        s._autolock["stepsize"] = None
        s._autolock["greater"] = None
        s._autolock["amplitude"] = None
        s._autolock["offset"] = None
        s._autolock["threshold"] = None
        s._autolock["threshold_break"] = None
        s._autolock["relock"] = None

        s._readLockControl()

        self.assertAlmostEqual(s.lockStepsize, 0.00021)
        self.assertEqual(s._autolock["greater"], 0)
        self.assertAlmostEqual(s.lockAmplitude, 1.4)
        self.assertAlmostEqual(s.lockOffset, 2.8)
        self.assertAlmostEqual(s.lockThreshold, 3.4)
        self.assertAlmostEqual(s.lockThresholdBreak, 3.1)
        self.assertEqual(s._autolock["relock"], 1)

    def test_apply_threshold_and_sensitivity(self):
        self.s.lockThreshold = 1.03
        self.s.lockThresholdBreak = 1.01
        self.s.auxSensitivity = 3
        self.s.inputSensitivity = 2
        self.assertEqual(self.s.auxSensitivity, 3)
        self.assertEqual(self.s.inputSensitivity, 2)

        dic = self.s.getSettingsDict()
        s = Servo(3, MockADwin(), applySettings=dic)

        dic["_state"]["auxSensitivity"] = 2
        self.s.loadSettings(dic)
        self.assertEqual(self.s._state["auxSensitivity"], 2)
        self.assertEqual(self.s.auxSensitivity, 2)

        self.assertAlmostEqual(s.lockThreshold, 1.03)
        self.assertAlmostEqual(s.lockThresholdBreak, 1.01)
        self.assertEqual(s.auxSensitivity, 3)
        self.assertEqual(s.inputSensitivity, 2)
