# pylint: disable=protected-access,missing-function-docstring,missing-class-docstring
import logging as log
from pathlib import Path
from time import sleep

import numpy as np
import pandas as pd
from openqlab import io

from nqontrol.general import errors, helpers, settings

from .test_servo import TestServoTemplate

log.basicConfig(
    format="%(levelname)s: %(module)s.%(funcName)s: %(message)s", level="INFO"
)

settings.NUMBER_OF_SERVOS = 8
settings.NUMBER_OF_MONITORS = 8


class TestServoAutolock(TestServoTemplate):
    def test_lock_getters_and_setters(self):
        self.s.lockSearch = 0
        with self.assertRaises(TypeError):
            self.s.lockSearch = "Hello there!"
        with self.assertRaises(TypeError):
            self.s.locked = "General Kenobi"
        with self.assertRaises(TypeError):
            self.s.relock = "A surprise to be sure"
        with self.assertRaises(TypeError):
            self.s.lockGreater = "But a welcome one"
        with self.assertRaises(TypeError):
            self.s.lockRampmode = "I hate sand"
        with self.assertRaises(TypeError):
            self.s.lockThresholdBreak = "It's rough and coarse."
        self.assertEqual(0, self.s.lockSearch)
        with self.assertRaises(TypeError):
            self.s.lockThreshold = "wrong type"
        with self.assertRaises(TypeError):
            self.s.lockFrequency = "mad typing"
        with self.assertRaises(ValueError):
            self.s.lockFrequency = -5
        with self.assertRaises(TypeError):
            self.s.lockStepsize = "Marble"
        with self.assertRaises(ValueError):
            self.s.lockStepsize = 100.0
        self.s.lockThreshold = 7
        self.s.lockThresholdBreak = 7
        self.s.lockGreater = 1
        self.s.lockAmplitude = 3
        self.s.lockOffset = 0
        self.s.lockSearch = 1
        # let's ensure that lock will still be running after changing range
        # also minimum value should be smaller than maximum value
        self.assertTrue(self.s.lockSearch)
        self.s.lockAmplitude = 10
        self.assertTrue(self.s.lockSearch)
        # more weird shennigans that really should not occur in use case
        self.s.relock = 1
        self.assertTrue(self.s.relock)
        self.s.lockThreshold = 10
        self.s.lockThresholdBreak = 10
        self.s.lockGreater = 0
        self.s.lockSearch = 0
        self.s.locked = True
        self.assertTrue(self.s.locked)
        self.s.lockGreater = False
        self.assertFalse(self.s.lockGreater)
        self.s.lockGreater = 1
        self.assertTrue(self.s.lockGreater)

    def test_servo_autolock(self):
        # set everything to off
        self.s.lockSearch = 0
        self.s.locked = 0
        self.assertEqual(self.s.lockSearch, self.s.locked)

        # settings so that lock should always find a lock point
        self.s.lockThreshold = 10
        self.s.lockThresholdBreak = 10
        self.s.lockFrequency = 10
        self.s.lockAmplitude = 5
        self.s.lockOffset = 0
        self.s.relock = 0
        self.s.lockGreater = 0
        log.debug(f"Lock control register   {bin(self.sd._lockControlRegister())}")

        self.s.lockSearch = 1
        sleep(0.1)
        # should have found the lock right away, has to turn off lock mode and turn on locked state
        self.assertTrue(self.s.locked)

        self.assertFalse(self.s.lockSearch)
        self.s.lockGreater = 1
        sleep(0.1)
        self.assertFalse(self.s.locked)

    def test_lock_searches_in_correct_range(self):
        self.s.locked = 0
        self.s.relock = 1
        # we'll start this before, to make sure that resetting the range does not result in the lock iterator being out of bounds!
        # this might happen, for example, if the new range is smalle than the current one and the search is still active
        self.s.lockSearch = 1
        self.s.lockGreater = True
        self.s.lockThreshold = 8
        self.s.lockThresholdBreak = 8
        adjust = 0
        # NOTE: if the lockFrequency is higher than the minimum, meaning each iteration step is greater than +1 or -1
        # it can happen that the lock will run slightly out of search range!
        # that is perfectly fine because as soon as it happens we just switch the sign of the iterator
        # the only case where we should not see this is (i believe) for range [-10, 10], because ADwin limits the output
        # however, internally the values can still be higher/lower
        # NOTE: Even though we do this, it will not even show up due to conversion inaccuracies most of the time
        # we check for "almost" equal values anyway, rounding them to the 3rd place
        # still, though a 255 lock search stepsize will be small compared to the 65535 values occupied by a 16 bit int
        # We can actually calulate 255 / 65535 to be ~0.00389
        # so the test might rarely fail with that high of a step size since ranges will differ in the 3rd digit
        if self.s.lockFrequency > settings.RAMP_FREQUENCY_MIN:
            adjust = helpers.convertFloat2Volt(
                helpers.convertFrequency2Stepsize(self.s.lockFrequency), signed=True
            )
        log.warning(("adjust", adjust))
        self.s.lockAmplitude = 1
        self.s.lockOffset = 4
        sleep(0.1)
        # let's test this a couple of times
        for _ in range(500):
            self.assertGreaterEqual(
                round(self.s._lockIter() * self.s.lockAmplitude + self.s.lockOffset, 2),
                round(3 - adjust, 2),
            )
            self.assertLessEqual(
                round(self.s._lockIter() * self.s.lockAmplitude + self.s.lockOffset, 2),
                round(5 + adjust, 2),
            )
        self.s.lockAmplitude = 1
        self.s.lockOffset = -5
        sleep(0.1)
        for _ in range(500):
            self.assertGreaterEqual(
                round(self.s._lockIter() * self.s.lockAmplitude + self.s.lockOffset, 2),
                round(-6 - adjust, 2),
            )
            self.assertLessEqual(
                round(self.s._lockIter() * self.s.lockAmplitude + self.s.lockOffset, 2),
                round(-4 + adjust, 2),
            )

    def test_lock_control_register(self):
        lcr1 = self.sd._lockControlRegister()
        log.debug(f"The old register for debugging: {bin(lcr1)}")
        self.s.lockSearch = 0
        self.s.relock = 0
        self.s.locked = 0
        self.s.lockGreater = 0
        self.s.lockRampmode = 0
        lcr2 = self.sd._lockControlRegister()
        gcr = self.sd._greaterControlRegister()
        log.debug(f"lcr2 lock register for debugging: {bin(lcr2)}")
        bitoffset = self.s._channel - 1
        lock = helpers.readBit(lcr2, bitoffset)
        locked = helpers.readBit(lcr2, bitoffset + 8)
        relock = helpers.readBit(lcr2, bitoffset + 16)
        greater = helpers.readBit(gcr, self.s._channel - 1)
        rampmode = helpers.readBit(gcr, 8 + self.s._channel - 1)
        self.assertEqual(lock, 0)
        self.assertEqual(locked, 0)
        self.assertEqual(relock, 0)
        self.assertEqual(greater, 0)
        self.assertEqual(rampmode, 0)
        # now the other way round
        # first, ensure a lock wont be found
        self.s.lockThreshold = 9
        self.s.lockThresholdBreak = 9
        self.s.lockGreater = 1
        self.s.lockRampmode = 0
        self.s.relock = 1
        self.s.lockSearch = 1
        lcr3 = self.sd._lockControlRegister()
        gcr = self.sd._greaterControlRegister()
        log.debug(f"lcr3 lock register for debugging: {bin(lcr3)}")
        lock = helpers.readBit(lcr3, bitoffset)
        locked = helpers.readBit(lcr3, bitoffset + 8)
        relock = helpers.readBit(lcr3, bitoffset + 16)
        greater = helpers.readBit(gcr, self.s._channel - 1)
        rampmode = helpers.readBit(gcr, 8 + self.s._channel - 1)
        self.assertFalse(locked)
        self.assertTrue(lock)
        self.assertTrue(relock)
        self.assertTrue(greater)
        self.assertFalse(rampmode)
        # let's have a lock found
        self.s.lockThreshold = 10
        self.s.lockThresholdBreak = 10
        self.s.lockGreater = 0
        sleep(0.1)
        lcr4 = self.sd._lockControlRegister()
        gcr = self.sd._greaterControlRegister()
        log.warning(
            f"lcr4 lock register for debugging: lcr {bin(lcr4)} gcr {bin(gcr)}, offset {bitoffset}"
        )
        lock = helpers.readBit(lcr4, bitoffset)
        locked = helpers.readBit(lcr4, bitoffset + 8)
        relock = helpers.readBit(lcr4, bitoffset + 16)
        greater = helpers.readBit(gcr, self.s._channel - 1)
        # lock should now be off if testing on a real device
        self.assertFalse(greater)
        self.assertTrue(relock)
        self.assertFalse(lock)
        self.assertTrue(locked)

    def test_lock_rampmode(self):
        # if we set up rampmode and use everything so that lock should be found, it should still keep searching
        # first reset everything
        self.s.lockSearch = 0
        self.s.locked = 0
        self.s.relock = 0
        # set things up
        self.s.lockThreshold = 9
        self.s.lockThresholdBreak = 9
        self.s.lockGreater = 0
        self.s.lockRampmode = 1
        # relock shouldn't affect rampmode
        self.s.relock = 1
        # start the search
        self.s.lockSearch = 1
        sleep(0.1)
        # should keep looking
        self.assertTrue(self.s.lockSearch)
        self.assertFalse(self.s.locked)
        self.assertTrue(self.s.relock)
        self.assertTrue(self.s.lockRampmode)
        # quick look at the register
        gcr = self.sd._greaterControlRegister()
        rampmode = helpers.readBit(gcr, 8 + self.s._channel - 1)
        self.assertTrue(rampmode)
        # now let's turn it off
        self.s.lockRampmode = 0
        sleep(0.1)
        # should find a lock
        self.assertFalse(self.s.lockSearch)
        self.assertTrue(self.s.locked)
        self.assertFalse(self.s.lockGreater)
        self.assertTrue(self.s.relock)
        # rampmode should retrigger search if coming from locked state
        self.s.lockRampmode = 1
        self.assertTrue(self.s.lockSearch)
        self.assertFalse(self.s.locked)
        # and should find lock again if turning off
        self.s.lockRampmode = 0
        sleep(0.1)
        self.assertFalse(self.s.lockSearch)
        self.assertTrue(self.s.locked)

    def test_lock_disables_input_output(self):
        # start infinite lock process
        self.s.lockThreshold = 10
        self.s.lockThresholdBreak = 10
        self.s.lockGreater = 1
        self.s.lockSearch = 1
        self.assertFalse(self.s.inputSw)
        self.assertFalse(self.s.outputSw)
        self.assertFalse(self.s.auxSw)
        sleep(0.1)
        self.s.lockGreater = 0
        sleep(0.1)
        self.assertTrue(self.s.locked)
        self.assertTrue(self.s.inputSw)
        self.assertTrue(self.s.outputSw)

    def test_lock_search_state(self):
        # enable infinite lock
        self.s.lockThreshold = 10
        self.s.lockThresholdBreak = 10
        self.s.lockGreater = 1
        self.s.lockSearch = 1
        sleep(0.1)
        self.assertTrue(self.s.lockSearch)

    def test_lock_found_state(self):
        self.s.lockThreshold = -8
        self.s.lockThresholdBreak = -8
        self.s.lockGreater = True
        self.s.lockSearch = 1
        sleep(0.1)
        self.assertTrue(self.s.locked)

    def test_lock_relock_starts_search(self):
        # infinite search
        self.s.lockThreshold = 8
        self.s.lockThresholdBreak = 8
        self.s.lockGreater = True
        self.s.locked = False
        self.s.relock = 1
        self.s.lockSearch = 1
        sleep(0.1)
        self.assertTrue(self.s.lockSearch)
        self.s.lockGreater = False
        sleep(0.1)
        self.assertFalse(self.s.lockSearch)
        self.assertTrue(self.s.locked)
        self.assertTrue(self.s.relock)
        self.s.lockGreater = True
        sleep(0.1)
        # this is the relevant part
        self.assertTrue(self.s.lockSearch)
        self.assertFalse(self.s.locked)
        self.assertTrue(self.s.relock)

    def test_lock_iter_default(self):
        self.s.lockSearch = 0
        self.s.locked = 0
        self.assertAlmostEqual(self.s._lockIter(), -2, places=3)

    def test_lock_iter_range(self):
        self.s.gain = 1
        self.s.offset = 0
        self.s.lockAmplitude = 0.5
        self.s.lockOffset = 7.5
        # some arbitrarily high value, no correlation with the searchrange (we expect to test with white noise)
        self.s.lockRampmode = 0
        self.s.lockThreshold = 8
        self.s.lockThresholdBreak = 8
        self.s.lockGreater = True
        self.s.relock = 0
        # start search
        self.s.lockSearch = True
        sleep(0.1)
        # we expect it to be in search state
        self.assertTrue(self.s.lockSearch)
        self.assertFalse(self.s.locked)
        self.assertTrue(-1 < self.s._lockIter() < 1)
        self.assertTrue(7 < 0.5 * self.s._lockIter() + 7.5 < 8)
        log.debug(f"iter {self.s._lockIter()}")
        log.warning(f"test lock output {self.s._testLockOutput()}")
        self.assertEqual(
            self.s.filters,
            [
                [1.0, 0.0, 0.0, 0.0, 0.0],
                [1.0, 0.0, 0.0, 0.0, 0.0],
                [1.0, 0.0, 0.0, 0.0, 0.0],
                [1.0, 0.0, 0.0, 0.0, 0.0],
                [1.0, 0.0, 0.0, 0.0, 0.0],
            ],
        )

        # switch to Greater = False, should "find" lock immediately
        self.s.lockGreater = False
        # let's see what happens to the output
        data = self.s.takeData()
        self.assertFalse(self.s.lockSearch)
        self.assertTrue(self.s.locked)
        data = data["output"].to_numpy()
        log.warning(data)
        self.assertTrue(
            7 < self.s.lockAmplitude * self.s._lockIter() + self.s.lockOffset < 8
        )
        log.warning(f"iter {self.s._lockIter()}")
        log.warning(f"test lock output {self.s._testLockOutput()}")
        # output should roughly stay within the same range as set for the search
        self.assertTrue(np.all((data > 6.9) & (data < 8.1)))

    def test_lock_frequency_does_not_break_search(self):
        self.s.gain = 1
        self.s.offset = 0
        self.s.lockAmplitude = 1.5
        self.s.lockOffset = 1.5
        self.s.lockThreshold = 9
        self.s.lockThresholdBreak = 9
        self.s.lockGreater = 1
        self.s.relock = 0
        self.s.lockSearch = 0
        self.s.lockFrequency = settings.RAMP_FREQUENCY_MAX
        self.s.lockSearch = 1
        sleep(0.05)
        self.assertTrue(self.s.lockSearch)
        self.assertFalse(self.s.locked)
        intrange = [
            (-1 - self.s.lockStepsize) * 1.5 + 1.5,
            (1 + self.s.lockStepsize) * 1.5 + 1.5,
        ]
        log.warning(f"intrange {intrange}")
        log.warning(f"iterator {self.s._lockIter()}")
        for _ in range(30):
            # trigger _par_special_functions on mockadwin
            self.assertTrue(self.s.lockSearch)
            position = 1.5 * self.s._lockIter() + 1.5
            log.warning(f"lock position {position}")
            self.assertTrue(intrange[0] <= position <= intrange[1])
            sleep(0.01)
        # additional tests on output
        data = self.s.takeData()
        self.assertTrue(self.s.lockSearch)
        data = data["output"].to_numpy()
        log.warning(f"data from readout {data}")
        log.warning(f"min {np.min(data)} max {np.min(data)}")
        self.assertTrue(np.all((data >= intrange[0]) & (data <= intrange[1])))

        # test same stuff for maximum range
        self.s.lockOffset = 0
        self.s.lockAmplitude = 10
        self.assertEqual(settings.RAMP_FREQUENCY_MAX, self.s.lockFrequency)
        self.assertTrue(self.s.lockSearch)
        intrange = [
            (-1 - self.s.lockStepsize) * 10.0 + 0.0,
            (1 + self.s.lockStepsize) * 10.0 + 0.0,
        ]
        log.warning(f"intrange {intrange}")
        for _ in range(30):
            position = 10 * self.s._lockIter()
            log.warning(f"iterator pos: {position}")
            self.assertTrue(intrange[0] <= position <= intrange[1])
            sleep(0.01)
        # data output has to remain in bounds though
        data = self.s.takeData()
        self.assertTrue(self.s.lockSearch)
        data = data["output"].to_numpy()
        log.warning(data)
        self.assertTrue(np.all((data >= -10) & (data <= 10)))

    def test_lock_off_state(self):
        self.s.lockThreshold = 8
        self.s.lockThresholdBreak = 8
        self.s.lockGreater = 1
        self.s.lockSearch = 1
        sleep(0.1)
        self.s.lockSearch = 0
        self.assertFalse(self.s.lockSearch)
        self.assertFalse(self.s.locked)

    def test_lock_commandline_usage(self):
        with self.assertRaises(TypeError):
            self.s.autolock("baby yoda", analysis=False)
        with self.assertRaises(TypeError):
            self.s.autolock(relock="luke", analysis=False)
        with self.assertRaises(TypeError):
            self.s.autolock(amplitude="star wars has bad writers", analysis=False)
        with self.assertRaises(TypeError):
            self.s.autolock(offset="and no character depth", analysis=False)
        with self.assertRaises(TypeError):
            self.s.autolock(frequency="j j abrams is to blame", analysis=False)
        with self.assertRaises(ValueError):
            self.s.autolock(amplitude=-3, analysis=False)
        with self.assertRaises(ValueError):
            self.s.autolock(offset=15, analysis=False)
        with self.assertRaises(ValueError):
            self.s.autolock(frequency=-1, analysis=False)

        self.assertIn(
            "hello there",
            self.s.autolock(relock=True, kenobi="hello there", analysis=False),
        )
        self.assertIn("no effect", self.s.autolock(relcok=False, analysis=False))
        self.assertIn(
            "No options changed",
            self.s.autolock(election="establishment", analysis=False),
        )
        self.s.autolock(False, analysis=False)
        self.assertFalse(self.s.lockSearch)

        self.assertNotIn(
            "No options changed.",
            self.s.autolock(
                relock=True,
                greater=False,
                searchrange=[2, 4],
                treshold=-0.4,
                analysis=False,
            ),
        )

        self.assertNotIn(
            "Additional arguments", self.s.autolock(relock=True, analysis=False)
        )

        self.assertIn("rampmode", self.s.autolock(rampmode=True, analysis=False))
        self.assertIn("amplitude", self.s.autolock(amplitude=1.24, analysis=False))
        self.assertIn("offset", self.s.autolock(offset=8, analysis=False))
        self.assertIn("frequency", self.s.autolock(frequency=4.4, analysis=False))

    def test_lock_stepsize(self):
        with self.assertRaises(TypeError):
            self.s.lockFrequency = "achem"
        with self.assertRaises(ValueError):
            self.s.lockFrequency = -5
        step = helpers.convertFrequency2Stepsize(30)
        self.s.lockFrequency = 30
        self.assertAlmostEqual(
            step, helpers.convertFrequency2Stepsize(self.s.lockFrequency), places=3
        )

    def test_lock_offset_amplitude(self):
        self.s.lockOffset = 3.32
        self.s.lockThreshold = 4
        self.s.lockThresholdBreak = 4
        self.s.lockAmplitude = 1.5
        self.assertAlmostEqual(self.s.lockOffset, 3.32, places=3)
        self.assertAlmostEqual(self.s.lockThreshold, 4, places=3)
        self.assertAlmostEqual(self.s.lockThresholdBreak, 4, places=3)
        self.assertAlmostEqual(self.s.lockAmplitude, 1.5, places=3)
        with self.assertRaises(TypeError):
            self.s.lockAmplitude = "test123"
        with self.assertRaises(TypeError):
            self.s.lockOffset = "fail"
        with self.assertRaises(ValueError):
            self.s.lockOffset = -12
        with self.assertRaises(ValueError):
            self.s.lockOffset = -15
        with self.assertRaises(ValueError):
            self.s.lockAmplitude = -1.2
        with self.assertRaises(ValueError):
            self.s.lockAmplitude = 11

    def test_lock_analysis(self):

        p = Path.cwd() / "src" / "tests" / "test_servo" / "testdata" / "opo_scan.csv"
        data = io.read(p)
        data = data.rename(
            columns={"opo_scan_1": "input", "opo_scan_2": "aux", "opo_scan_3": "output"}
        )
        # make sure analysis wont change settings
        self.s.lockThreshold = -10
        self.s.lockThresholdBreak = -10
        self.s.lockGreater = 1
        self.s.lockAmplitude = 4
        self.s.lockOffset = -3
        self.s.lockFrequency = 4
        # first run with some prepared data, peaks are minima
        self.s.lockAnalysis(testdata=data)
        self.assertEqual(self.s.lockAmplitude, 4)
        self.assertAlmostEqual(self.s.lockOffset, -3, places=3)
        self.assertNotEqual(self.s.lockThreshold, -10)
        self.assertNotEqual(self.s.lockThresholdBreak, -10)
        self.assertFalse(self.s.lockGreater)
        self.assertAlmostEqual(self.s.lockThreshold, -5.366, places=2)
        self.assertAlmostEqual(self.s.lockThresholdBreak, -4.033, places=2)
        # let's turn it around and make it a little smaller, should still work
        # peaks are maxima
        data["aux"] = data["aux"] * (-0.03)
        self.s.lockAnalysis(testdata=data)
        self.assertTrue(self.s.lockGreater)
        self.assertAlmostEqual(self.s.lockThreshold, 0.16, places=2)
        self.assertAlmostEqual(self.s.lockThresholdBreak, 0.121, places=2)

        # let's make sure it still works if the monitor has different outputs visible (realtime["ydata"]), aka "aux" is not selected
        old_ydata = self.s.realtime["ydata"]
        self.s.realtime["ydata"] = ["input"]
        self.s.lockAnalysis()
        self.s.realtime["ydata"] = old_ydata

        # construct data where the analysis will fail
        data = pd.DataFrame()
        data["aux"] = np.random.randn(1000)

        with self.assertRaises(errors.AutoLockAnalysisError):
            self.s.lockAnalysis(testdata=data, raiseErrors=True)

        # empy data
        with self.assertRaises(errors.DeviceError):
            self.s.lockAnalysis(testdata=pd.DataFrame(), raiseErrors=True)
        # should only log warning
        self.s.lockAnalysis(testdata=pd.DataFrame(), raiseErrors=False)

        ############### test prepareLockData seperately
        # should leave previous values intact
        self.s.lockSearch = 0
        self.s.lockRampmode = 0
        # needs to be preserved for other tests
        old_ydata = self.s.realtime["ydata"]
        self.s.realtime["ydata"] = ["input", "output"]
        _ = self.s._prepareLockData()
        self.assertFalse(self.s.lockSearch)
        self.assertFalse(self.s.lockRampmode)
        self.s.lockRampmode = 1
        _ = self.s._prepareLockData()
        self.assertFalse(self.s.lockSearch)
        self.assertTrue(self.s.lockRampmode)
        self.assertEqual(self.s.realtime["ydata"], ["input", "output"])
        self.s.realtime["ydata"] = old_ydata

    def test_set_and_get_threshold(self):
        self.s.lockThresholdBreak = 1.642
        self.assertAlmostEqual(self.s._autolock["threshold_break"], 1.642)
        self.s.lockThreshold = 5.123
        self.assertAlmostEqual(self.s._autolock["threshold"], 5.123)

        self.s._autolock["threshold_break"] = 9.431
        self.assertAlmostEqual(self.s.lockThresholdBreak, 1.642)
        self.s._autolock["threshold"] = -2.95
        self.assertAlmostEqual(self.s.lockThreshold, 5.123)
