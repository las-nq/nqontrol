# pylint: disable=protected-access
import logging as log

from nqontrol.general import settings

from .test_servo import TestServoTemplate

log.basicConfig(
    format="%(levelname)s: %(module)s.%(funcName)s: %(message)s", level="INFO"
)

settings.NUMBER_OF_SERVOS = 8
settings.NUMBER_OF_MONITORS = 8


class TestServoRamp(TestServoTemplate):
    def test_convertFrequencyBack(self):
        for n in range(1, 255):
            self.s.enableFifo(n)
            f = self.s.lockFrequency
            self.s.lockFrequency = f
            self.assertEqual(self.s.fifoStepsize, n)

    def test_calculateRefreshTime(self):
        self.s._fifo["stepsize"] = settings.SAMPLING_RATE
        self.s._calculateRefreshTime()
        self.assertAlmostEqual(self.s.realtime["refreshTime"], 1500)

        self.s._fifo["stepsize"] = 1
        self.s._calculateRefreshTime()
        self.assertAlmostEqual(self.s.realtime["refreshTime"], self.s._MIN_REFRESH_TIME)

    def test_rampAmplitude(self):
        self.s.lockAmplitude = 5.6
        self.assertEqual(self.s.lockAmplitude, 5.6)
        self.s.lockRampmode = 1
        self.s.lockSearch = 1
        self.s.lockAmplitude = 8.6
        self.assertEqual(self.s.lockAmplitude, 8.6)
        self.assertTrue(self.s.lockSearch)
        self.assertTrue(self.s.lockRampmode)
        with self.assertRaises(ValueError):
            self.s.lockAmplitude = 10.1
        with self.assertRaises(ValueError):
            self.s.lockAmplitude = -0.1

    def test_rampFrequency(self):
        self.s.lockFrequency = 47.5
        self.assertAlmostEqual(self.s.lockFrequency, 47.5, places=0)
        self.s.lockRampmode = 1
        self.s.lockSearch = 1
        self.s.lockFrequency = 22

    def test_enableRamp(self):
        self.s.enableRamp(amplitude=1, offset=-2, frequency=3, enableFifo=True)
        self.s.disableFifo()
        self.s.enableRamp(enableFifo=False)
        self.assertFalse(self.s.fifoEnabled)
