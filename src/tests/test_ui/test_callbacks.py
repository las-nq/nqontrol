# pylint: disable=missing-function-docstring,missing-class-docstring,protected-access
import logging as log
import unittest
from base64 import b64encode
from os import path, remove, stat
from tempfile import mkstemp
from time import sleep

import _controller as c
from dash.exceptions import PreventUpdate
from openqlab import io

from nqontrol.gui import dependencies
from nqontrol.gui.widgets.second_order_section import _callbacks

from nqontrol.general import settings  # isort:skip


log.basicConfig(
    format="%(levelname)s: %(module)s.%(funcName)s: %(message)s", level="INFO"
)

DEVICE = dependencies.DEVICE
SERVO = DEVICE.servo


class MockContext:
    def __init__(self, value):
        self.value = value
        self.triggered = {0: {"prop_id": f"{self.value}.bla"}}


class TestCallbacks(unittest.TestCase):
    def test_call_servo_name(self):
        self.assertNotEqual(SERVO(1).name, "testing")
        name = c.callServoName(1, 1, "testing")
        self.assertEqual(name, "testing")
        self.assertEqual(SERVO(1).name, name)
        self.assertNotEqual(SERVO(2).name, name)
        with self.assertRaises(PreventUpdate):
            c.callServoName(1, None, "bla")

    def test_call_input_sensitivity(self):
        self.assertEqual(
            c.callInputSensitivity(0, 3), f"Input sensitivity (Limit: 10 V, Mode: 0)"
        )
        self.assertEqual(
            c.callInputSensitivity(1, 5), f"Input sensitivity (Limit: 5 V, Mode: 1)"
        )
        self.assertEqual(
            c.callInputSensitivity(2, 7), f"Input sensitivity (Limit: 2.5 V, Mode: 2)"
        )
        self.assertEqual(
            c.callInputSensitivity(3, 8), f"Input sensitivity (Limit: 1.25 V, Mode: 3)"
        )

        self.assertEqual(SERVO(3).inputSensitivity, 0)
        self.assertEqual(SERVO(5).inputSensitivity, 1)
        self.assertEqual(SERVO(7).inputSensitivity, 2)
        self.assertEqual(SERVO(8).inputSensitivity, 3)

    def test_call_aux_sensitivity(self):
        self.assertEqual(
            c.callAuxSensitivity(0, 3), f"Aux sensitivity (Limit: 10 V, Mode: 0)"
        )
        self.assertEqual(
            c.callAuxSensitivity(1, 5), f"Aux sensitivity (Limit: 5 V, Mode: 1)"
        )
        self.assertEqual(
            c.callAuxSensitivity(2, 7), f"Aux sensitivity (Limit: 2.5 V, Mode: 2)"
        )
        self.assertEqual(
            c.callAuxSensitivity(3, 8), f"Aux sensitivity (Limit: 1.25 V, Mode: 3)"
        )

        self.assertEqual(SERVO(3).auxSensitivity, 0)
        self.assertEqual(SERVO(5).auxSensitivity, 1)
        self.assertEqual(SERVO(7).auxSensitivity, 2)
        self.assertEqual(SERVO(8).auxSensitivity, 3)

    def test_call_reboot(self):
        self.assertEqual(settings.SAMPLING_RATE, 200e3)
        sleep(1)
        self.assertGreater(DEVICE.timestamp, 0)
        self.assertIsNone(c.callReboot(None), None)  # Should not do anything
        self.assertGreater(DEVICE.timestamp, 0)
        self.assertEqual(c.callReboot(1), "Rebooted successfully.")  # Should reboot
        self.assertEqual(DEVICE.timestamp, 0)

    def test_call_save(self):
        _, save_path = mkstemp(suffix=".json")
        with self.assertRaises(PreventUpdate):
            c.callSave(None, save_path)
        self.assertEqual(stat(save_path).st_size, 0)
        self.assertEqual(c.callSave(1, save_path), f"Saved as {save_path}.")
        self.assertEqual(c.callSave(1, None), "Saved as untitled_device.json.")
        with self.assertRaises(PreventUpdate):
            # pylint: disable=anomalous-backslash-in-string
            c.callSave(3, "!~&x`^°<>:-,\/{")
        self.assertGreater(stat(save_path).st_size, 0)
        remove(save_path)

        if path.exists("untitled_device.json"):
            remove("untitled_device.json")

        if path.exists("untitled_device.json"):
            remove("untitled_device.json")

    def test_call_monitor_update(self):
        with self.assertRaises(KeyError):
            c.callMonitorUpdate(1, ["input", "wrong", "output"])
        with self.assertRaises(PreventUpdate):
            c.callMonitorUpdate(1, [])

        fig = c.callMonitorUpdate(1, ["input"])
        self.assertEqual(len(fig["data"]), 1)
        self.assertEqual(fig["data"][0].name, "input")

        fig = c.callMonitorUpdate(1, ["input", "aux", "output"])
        self.assertEqual(len(fig["data"]), 3)
        self.assertEqual(fig["data"][0].name, "input")
        self.assertEqual(fig["data"][1].name, "aux")
        self.assertEqual(fig["data"][2].name, "output")

    def test_call_monitor_update_channels(self):
        self.assertIn("input", c.callMonitorUpdateChannels(1, ["input"]))
        out = c.callMonitorUpdateChannels(1, ["input", "aux", "output"])
        self.assertIn("input", out)
        self.assertIn("aux", out)
        self.assertIn("output", out)

        self.assertEqual(c.callMonitorUpdateChannels(1, []), "Empty channels")

    def test_call_workload_timestamp(self):
        if DEVICE.deviceNumber == 0:
            self.assertIn("42", c.callWorkloadTimestamp())
            self.assertIn("Timestamp: 0", c.callWorkloadTimestamp())

    def test_call_offset(self):
        self.assertEqual(c.callOffset(3, 4.2), "Offset (4.200 V)")
        self.assertAlmostEqual(SERVO(3).offset, 4.2)
        self.assertAlmostEqual(SERVO(3).offset, 4.2)
        with self.assertRaises(PreventUpdate):
            c.callOffset(3, "f")
        self.assertAlmostEqual(SERVO(3).offset, 4.2)

    def test_call_gain(self):
        fake_context = MockContext("gain_7")
        self.assertEqual(c.callGain(fake_context, 7, 4.7), "Gain (4.700)")
        self.assertEqual(c.callGain(fake_context, 7, 4.7), "Gain (4.700)")
        for i in range(1, settings.NUMBER_OF_SERVOS + 1):
            SERVO(i).gain = float(i)
            self.assertEqual(SERVO(i).gain, float(i))
            fake_context = MockContext("different")
            self.assertEqual(c.callGain(fake_context, i, 2.3), f"Gain ({i:.3f})")
            self.assertEqual(SERVO(i).gain, float(i))

        with self.assertRaises(PreventUpdate):
            fake_context = MockContext("gain_7")
            c.callGain(fake_context, 7, "x")

    def test_call_servodesign_gain(self):
        self.assertEqual(c.callServoDesignGain(3), "Gain (3)")
        self.assertEqual(c.callServoDesignGain(3), "Gain (3)")
        self.assertEqual(DEVICE.servoDesign.gain, 3)
        self.assertEqual(c.callServoDesignGain(0.8), "Gain (0.8)")
        self.assertEqual(DEVICE.servoDesign.gain, 0.8)
        with self.assertRaises(PreventUpdate):
            c.callServoDesignGain("jfk2")

    def test_call_servo_channels(self):
        c.callServoChannels(4, ["input"])
        self.assertTrue(SERVO(4).inputSw)
        self.assertFalse(SERVO(4).offsetSw)
        self.assertFalse(SERVO(4).auxSw)
        self.assertFalse(SERVO(4).outputSw)

        c.callServoChannels(4, ["offset"])
        self.assertFalse(SERVO(4).inputSw)
        self.assertTrue(SERVO(4).offsetSw)
        self.assertFalse(SERVO(4).auxSw)
        self.assertFalse(SERVO(4).outputSw)

        c.callServoChannels(4, ["aux"])
        self.assertFalse(SERVO(4).inputSw)
        self.assertFalse(SERVO(4).offsetSw)
        self.assertTrue(SERVO(4).auxSw)
        self.assertFalse(SERVO(4).outputSw)

        c.callServoChannels(4, ["output"])
        self.assertFalse(SERVO(4).inputSw)
        self.assertFalse(SERVO(4).offsetSw)
        self.assertFalse(SERVO(4).auxSw)
        self.assertTrue(SERVO(4).outputSw)

        c.callServoChannels(4, ["input", "offset", "aux", "output"])
        self.assertTrue(SERVO(4).inputSw)
        self.assertTrue(SERVO(4).offsetSw)
        self.assertTrue(SERVO(4).auxSw)
        self.assertTrue(SERVO(4).outputSw)

        c.callServoChannels(4, [])
        self.assertFalse(SERVO(4).inputSw)
        self.assertFalse(SERVO(4).offsetSw)
        self.assertFalse(SERVO(4).auxSw)
        self.assertFalse(SERVO(4).outputSw)

    def test_call_apply_servodesign(self):
        self.assertIn("Applied ServoDesign on 5", c.callApplyServoDesign(5, 1))
        self.assertIn("Applied ServoDesign on 3", c.callApplyServoDesign(3, 9))
        with self.assertRaises(PreventUpdate):
            c.callApplyServoDesign(7, None)
        with self.assertRaises(IndexError):
            c.callApplyServoDesign(settings.NUMBER_OF_SERVOS + 1, 3)

    def test_call_apply_filter_labels(self):
        DEVICE.servoDesign.clear()
        DEVICE.servoDesign.integrator(400)
        DEVICE.servoDesign.notch(900)

        _, labels, _ = c.callApplyFiltersToServo(1, 1, 3)
        self.assertIn("400", labels[0]["label"])
        _, labels, _ = c.callApplyFiltersToServo(4, 4, 3)
        self.assertIn("900", labels[1]["label"])
        _, labels, _ = c.callApplyFiltersToServo(6, 6, 3)
        self.assertIn("Int", labels[0]["label"])
        _, labels, _ = c.callApplyFiltersToServo(7, 7, 3)
        self.assertIn("Notch", labels[1]["label"])
        _, labels, _ = c.callApplyFiltersToServo(7, 7, 3)
        self.assertIn("Filter 4", labels[4]["label"])
        with self.assertRaises(PreventUpdate):
            c.callApplyFiltersToServo(2, 3, 1)
        with self.assertRaises(PreventUpdate):
            c.callApplyFiltersToServo(2, 2, None)
        with self.assertRaises(IndexError):
            c.callApplyFiltersToServo(
                settings.NUMBER_OF_SERVOS + 1, settings.NUMBER_OF_SERVOS + 1, 2
            )
        with self.assertRaises(IndexError):
            c.callApplyFiltersToServo(0, 0, 2)

    def test_call_toggle_servo_filters(self):
        SERVO(2).servoDesign.clear()
        for _ in range(settings.NUMBER_OF_FILTERS):
            SERVO(2).servoDesign.integrator(400)
        c.callToggleServoFilters(2, [0, 3])
        self.assertTrue(SERVO(2).filterStates[0])
        self.assertFalse(SERVO(2).filterStates[1])
        self.assertFalse(SERVO(2).filterStates[4])
        self.assertTrue(SERVO(2).filterStates[3])
        self.assertListEqual(SERVO(2).filterStates, [True, False, False, True, False])
        sd = SERVO(2).servoDesign
        sd.clear()
        sd.integrator(5)
        # we want this called in a case where some part of the ServoDesign is empty
        c.callToggleServoFilters(2, [])
        self.assertFalse(SERVO(2).filterStates[3])

    def test_gain_should_not_change_on_filter_switch(self):
        s = SERVO(2)
        s.servoDesign.clear()
        s.servoDesign.gain = 83
        for _ in range(settings.NUMBER_OF_FILTERS):
            s.servoDesign.integrator(400)

        s.applyServoDesign()

        self.assertListEqual(s.filterStates, [True, True, True, True, True])
        self.assertEqual(s.gain, 83)
        s.gain = 0.03

        c.callToggleServoFilters(2, [0, 3])

        self.assertListEqual(s.filterStates, [True, False, False, True, False])
        self.assertEqual(s.gain, 0.03)

    # TODO bring test to work
    @unittest.skip("Test not working for now")
    def test_call_plant_parse(self):
        tests_dir = path.dirname(path.abspath(__file__))
        filename = f"{tests_dir}/../support_files/fra.csv"
        with open(filename, encoding="cp1252") as f:
            plant = f.read()
        plant_b64 = b64encode(plant.encode("utf-8"))
        upload = f"content type,{plant_b64}"

        self.assertIsNone(DEVICE.servoDesign.plant)
        c.callPlantParse(filename, upload, 1, "x", "x")
        self.assertIsNotNone(DEVICE.servoDesign.plant)

    def test_call_plot_servo_design(self):
        DEVICE.servoDesign.clear()
        fig = c.callPlotServoDesign()

        DEVICE.servoDesign.integrator(500)
        fig = c.callPlotServoDesign()

        tests_dir = path.dirname(path.abspath(__file__))
        df = io.read(f"{tests_dir}/../support_files/fra.csv")
        DEVICE.servoDesign.plant = df
        fig = c.callPlotServoDesign()
        self.assertEqual(fig["layout"]["yaxis1"]["title"]["text"], "Amplitude (dB)")
        self.assertEqual(fig["layout"]["yaxis2"]["title"]["text"], "Phase (Hz)")

    def test_call_filter_description(self):
        self.assertEqual(c.callFilterDescription("", None, None, 0), "")
        self.assertEqual(
            c.callFilterDescription("Integrator", None, None, 0), "Main value expected."
        )
        self.assertIn(
            "Diff 433 Hz", c.callFilterDescription("Differentiator", "433", None, 0)
        )
        self.assertEqual(
            c.callFilterDescription("Integrator", 2, 0, 1),
            "Overflow error, inf in human_readable.",
        )
        self.assertEqual(
            c.callFilterDescription("Lowpass", 2, 0, 0),
            "Will divide by 0 and create a black hole.",
        )
        self.assertEqual("", c.callFilterDescription(None, 1, 2, 3))
        self.assertIn("Notch 833 Hz", c.callFilterDescription("Notch", "833", "900", 0))
        self.assertEqual(
            c.callFilterDescription("Differentiator", "jdsfk", None, 0),
            "Invalid main value.",
        )
        self.assertEqual(
            c.callFilterDescription("Differentiator", "433", "dfjs", 0),
            "Invalid secondary value.",
        )
        with self.assertRaises(PreventUpdate):
            _ = c.callFilterDescription("Müsli Müsli Mjam Mjam Mjam", "5", 2, 0)
        self.assertEqual(
            c.callFilterDescription("Notch", 0, 50, 0),
            "Overflow error, inf in human_readable.",
        )

    def test_call_lock_relock(self):
        self.assertEqual(c.callLockRelock(True, 1), "Relock: 1")
        self.assertTrue(c.getLockRelock(1))
        self.assertEqual(c.callLockRelock(False, 1), "Relock: 0")
        # call it with something else, should stay the
        with self.assertRaises(TypeError):
            _ = c.callLockRelock("test", 1)
        self.assertFalse(c.getLockRelock(1))
        s = DEVICE.servo(3)
        s.relock = 0
        self.assertFalse(c.getLockRelock(3))
        s.relock = 1
        self.assertTrue(c.getLockRelock(3))

    def test_get_lock_amp_offset(self):
        DEVICE.servo(4).lockAmplitude = 3.41
        DEVICE.servo(4).lockOffset = -5
        self.assertEqual(c.getLockAmplitude(4), 3.41)
        self.assertEqual(c.getLockOffset(4), -5.00)

    def test_get_lock_frequency(self):
        DEVICE.servo(2).lockFrequency = 10
        DEVICE.servo(5).lockFrequency = 4.5
        self.assertEqual(c.getLockFrequency(2), 10)

    def test_call_lock_state(self):
        s = DEVICE.servo(1)
        # make sure lock will never happen
        s.lockGreater = 1
        s.lockThreshold = 8
        s.lockThresholdBreak = 8
        ctxt = MockContext("Button")
        ctxt.triggered["1"] = "something"
        log.warning(ctxt.triggered)
        # with a context with more than one trigger, nothing should happen
        with self.assertRaises(PreventUpdate):
            c.callLockState(ctxt, 2, 1)
        ctxt = MockContext("Button")
        s.locked = 0
        s.lockSearch = 0
        # without clicks, nothing should be changed
        self.assertEqual(c.callLockState(ctxt, None, 1), "Turn on")
        self.assertFalse(s.locked)
        self.assertFalse(s.lockSearch)
        # turn off a search
        s.lockSearch = 1
        self.assertEqual(c.callLockState(ctxt, 4, 1), "Turn on")
        self.assertEqual(s.lockSearch, 0)
        # turn it back on
        self.assertEqual(c.callLockState(ctxt, 1, 1), "Turn off")
        self.assertEqual(s.lockSearch, 1)

    def test_get_max_filters(self):
        self.assertEqual(c.getMaxFilters(), settings.NUMBER_OF_FILTERS)
        self.assertEqual(c.getMaxFilters(), DEVICE.servoDesign.MAX_FILTERS)

    def test_get_active_filters(self):
        SERVO(4).servoDesign.clear()
        self.assertListEqual(c.getActiveFilters(4), [])
        SERVO(4).servoDesign.integrator(300)
        self.assertListEqual(c.getActiveFilters(4), [0])
        SERVO(4).servoDesign.differentiator(800)
        SERVO(4).servoDesign.notch(900)
        SERVO(4).servoDesign.notch(1400)
        SERVO(4).servoDesign.notch(1900)
        self.assertListEqual(c.getActiveFilters(4), [0, 1, 2, 3, 4])

    def test_get_filter_labels(self):
        def check_labels(result, reference):
            self.assertEqual(len(result), len(reference))
            for i, r in enumerate(result):
                self.assertEqual(r["label"], reference[i])

        s_design = SERVO(4).servoDesign
        s_design.clear()
        check_labels(
            c.getFilterLabels(4),
            ["Filter 0", "Filter 1", "Filter 2", "Filter 3", "Filter 4"],
        )
        s_design.integrator(300)
        check_labels(
            c.getFilterLabels(4),
            [
                s_design.filters[0].description,
                "Filter 1",
                "Filter 2",
                "Filter 3",
                "Filter 4",
            ],
        )
        s_design.differentiator(800)
        s_design.notch(900)
        s_design.notch(1400)
        s_design.notch(1900)
        check_labels(
            c.getFilterLabels(4),
            [
                s_design.filters[0].description,
                s_design.filters[1].description,
                s_design.filters[2].description,
                s_design.filters[3].description,
                s_design.filters[4].description,
            ],
        )

    def test_get_current_save_name(self):
        old = settings.SETTINGS_FILE
        settings.SETTINGS_FILE = None
        self.assertEqual(c.getCurrentSaveName(), "")
        settings.SETTINGS_FILE = "test"
        self.assertEqual(c.getCurrentSaveName(), "test")
        settings.SETTINGS_FILE = old  # in case another test still needs it

    def test_get_servo_name(self):
        SERVO(1).name = "Albert"
        self.assertEqual(c.getServoName(1), "Albert")
        with self.assertRaises(TypeError):
            c.getServoName("hello")
        with self.assertRaises(ValueError):
            c.getServoName(10)

    def test_get_input_states(self):
        # servo lock states turn off input switches, so let's make sure they behave as expected
        SERVO(1).lockSearch = 0
        SERVO(1).locked = 0
        SERVO(1).inputSw = True
        SERVO(1).offsetSw = False
        self.assertEqual(c.getInputStates(1), ["input"])
        SERVO(4).offsetSw = True
        SERVO(6).inputSw = True
        self.assertEqual(c.getInputStates(1), ["input"])
        self.assertEqual(c.getInputStates(4), ["offset"])
        SERVO(4).inputSw = True
        self.assertEqual(c.getInputStates(4), ["input", "offset"])
        with self.assertRaises(TypeError):
            c.getInputStates("hello")
        with self.assertRaises(ValueError):
            c.getInputStates(10)

    def test_get_offset(self):
        for i in range(1, settings.NUMBER_OF_SERVOS + 1):
            SERVO(i).inputSensitivity = 0
            SERVO(i).offset = i
            self.assertAlmostEqual(c.getOffset(i), i, places=3)
        SERVO(2).offset = -4
        self.assertAlmostEqual(c.getOffset(2), -4, places=3)
        with self.assertRaises(TypeError):
            c.getOffset("hello")
        with self.assertRaises(ValueError):
            c.getOffset(10)

    def test_get_gain(self):
        SERVO(2).gain = 0.04
        SERVO(4).gain = -0.5
        self.assertAlmostEqual(c.getGain(2), 0.04, places=3)
        with self.assertRaises(TypeError):
            c.getGain("hello")
        with self.assertRaises(ValueError):
            c.getGain(10)

    def test_get_filter_enabled(self):
        sd = DEVICE.servoDesign
        sd.clear()
        _ = c.getFilterEnabled(2)  # None case
        for i in range(settings.NUMBER_OF_FILTERS):
            sd.integrator(5)
            sd.get(i).enabled = False
        sd.get(2).enabled = True
        sd.get(4).enabled = True
        self.assertTrue(c.getFilterEnabled(2))
        self.assertFalse(c.getFilterEnabled(0))
        self.assertFalse(c.getFilterEnabled(1))
        self.assertTrue(c.getFilterEnabled(4))

    def test_get_filter_dropdown(self):
        sd = DEVICE.servoDesign
        sd.clear()
        sd.integrator(5)
        sd.notch(100)
        sd.lowpass(5)
        sd.lowpass(100)
        self.assertEqual(c.getFilterDropdown(0), "Integrator")
        self.assertEqual(c.getFilterDropdown(1), "Notch")
        self.assertEqual(c.getFilterDropdown(2), "Lowpass")
        self.assertEqual(c.getFilterDropdown(3), "Lowpass")
        self.assertEqual(c.getFilterDropdown(4), "")

    def test_apply_callback_bug_on_servo_1(self):
        # NOTE: I'll just leave the comments for the future
        # this was due to a bug where applying a servo design would work on all channels except channel 1
        # for channel 1 it broke all servoDesign values (at least the gains) even for other channels
        # let's try and reproduce
        # first thing that gets called is:
        DEVICE.servoDesign.gain = 0.4
        DEVICE.servo(2).gain = 1
        _ = c.callApplyServoDesign(2, 1)
        self.assertEqual(DEVICE.servoDesign.gain, 0.4)
        self.assertEqual(DEVICE.servo(2).gain, 0.4)
        _, _, _ = c.callApplyFiltersToServo(2, 2, 3)
        self.assertEqual("", c.callToggleServoFilters(2, []))
        ctxt = MockContext("gainStore_2")
        self.assertEqual(c.callGain(ctxt, 2, 1), f"Gain (0.400)")
        # now let's do the same thing for servo 1
        DEVICE.servo(1).gain = 1
        DEVICE.servoDesign.gain = 1.2
        self.assertEqual(DEVICE.servo(1).gain, 1.00)
        _ = c.callApplyServoDesign(1, 2)
        self.assertEqual(DEVICE.servoDesign.gain, 1.2)
        self.assertEqual(
            DEVICE.servo(1).gain, 1.2
        )  # ok so it currently fails here UPDATE: should now work
        # let's dig a little deeper: callApplyServoDesign is basically just this line:
        DEVICE.servo(1).applyServoDesign(DEVICE.servoDesign)
        self.assertEqual(DEVICE.servo(1).servoDesign, DEVICE.servoDesign)
        # possibly some error in the assignments on the device itself
        # what happens in servo.applyServoDesign?
        # the possibly faulty lines are basically:
        # self.gain = discreteServoDesign["gain"]
        # self.filters = filters
        # self.filterStates = filtersEnabled
        # but when checking the values in logger, everything seems normal
        # so stuff has to go wrong within those methods
        DEVICE.servo(1).gain = 1.2  # let's try this first
        self.assertEqual(1.2, DEVICE.servo(1).gain)
        # works, so next comes the filters
        DEVICE.servo(1).filters = [
            [1.0, 0, 0, 0, 0],
            [1.0, 0, 0, 0, 0],
            [1.0, 0, 0, 0, 0],
            [1.0, 0, 0, 0, 0],
            [1.0, 0, 0, 0, 0],
        ]
        self.assertEqual(
            [
                [1.0, 0, 0, 0, 0],
                [1.0, 0, 0, 0, 0],
                [1.0, 0, 0, 0, 0],
                [1.0, 0, 0, 0, 0],
                [1.0, 0, 0, 0, 0],
            ],
            DEVICE.servo(1).filters,
        )
        # what about the gain
        self.assertEqual(
            1.2, DEVICE.servo(1).gain
        )  # fails here UPDATE: should now work

        #### WE HAVE A WINNER ####
        # ok so there are two things unaccounted for, one is the sending the data to adwin
        # when logging the data doubles from the mockdevice i get something a little fishy
        ################# log before setting
        # _a = [1.2, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0]
        ################# log after setting
        # _b = [1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0]
        # NOTE: mistake was that MockADwin did not initialize the data_double fields correctly!
        self.assertEqual(
            DEVICE.servo(2).gain, 0.4
        )  # and this one fails, too UPDATE: should work now

        _, _, _ = c.callApplyFiltersToServo(1, 1, 3)
        self.assertEqual("", c.callToggleServoFilters(2, []))
        ctxt = MockContext("gainStore_1")
        self.assertEqual(c.callGain(ctxt, 1, 3), f"Gain (1.200)")
        self.assertEqual(DEVICE.servo(2).gain, 0.4)
        self.assertEqual(DEVICE.servo(1).gain, 1.2)

    def test_get_filter_main_par(self):
        sd = DEVICE.servoDesign
        sd.clear()
        sd.integrator(5)
        sd.notch(100)
        sd.lowpass(5)
        sd.lowpass(100)
        self.assertEqual(c.getFilterMainPar(4), "")
        self.assertEqual(c.getFilterMainPar(0), 5)
        self.assertEqual(c.getFilterMainPar(3), 100)

    def test_get_filter_second_par(self):
        sd = DEVICE.servoDesign
        sd.clear()
        sd.integrator(5)
        sd.notch(100)
        sd.lowpass(5, 0.98)
        sd.notch(100, 0.5)
        self.assertEqual(c.getFilterSecondPar(4), "")
        self.assertEqual(c.getFilterSecondPar(1), 1)
        self.assertEqual(c.getFilterSecondPar(3), 0.5)

    def test_get_filter_description(self):
        sd = DEVICE.servoDesign
        sd.clear()
        sd.integrator(5)
        sd.notch(100, 0.4)
        self.assertIsNone(c.getFilterDescription(3))
        self.assertIn("5 Hz", c.getFilterDescription(0))
        self.assertIn("100", c.getFilterDescription(1))
        self.assertIn(".4", c.getFilterDescription(1))

    def test_get_output_states(self):
        # make sure lock is off so states wont be affected
        SERVO(2).locked = 0
        SERVO(2).lockSearch = 0
        # play with some switches
        SERVO(2).auxSw = False
        SERVO(2).outputSw = False
        self.assertEqual(c.getOutputStates(2), [])
        SERVO(2).outputSw = True
        self.assertIn("output", c.getOutputStates(2))
        SERVO(2).auxSw = True
        self.assertEqual(c.getOutputStates(2), ["aux", "output"])

    def test_get_input_sensitivity(self):
        SERVO(2).inputSensitivity = 1
        SERVO(4).inputSensitivity = 2
        self.assertEqual(c.getInputSensitivity(2), 1)
        SERVO(2).inputSensitivity = 0
        self.assertEqual(c.getInputSensitivity(2), 0)

    def test_get_aux_sensitivity(self):
        SERVO(4).auxSensitivity = 2
        SERVO(2).auxSensitivity = 1
        self.assertEqual(c.getAuxSensitivity(1), 0)
        self.assertEqual(c.getAuxSensitivity(4), 2)

    def test_get_monitors_servo(self):
        for i in range(1, settings.NUMBER_OF_SERVOS + 1):
            DEVICE.disableMonitor(i)
        log.warning(DEVICE.monitors)
        DEVICE.enableMonitor(1, 2, "input")
        DEVICE.enableMonitor(2, 4, "aux")
        self.assertIsNone(c.getMonitorsServo(3))
        self.assertEqual(c.getMonitorsServo(1), 2)
        self.assertEqual(c.getMonitorsServo(2), 4)

    def test_get_monitors_card(self):
        log.warning(DEVICE.monitors)
        DEVICE.enableMonitor(1, 2, "input")
        DEVICE.enableMonitor(2, 4, "aux")
        self.assertIsNone(c.getMonitorsCard(0))
        self.assertEqual(c.getMonitorsCard(1), "input")
        self.assertEqual(c.getMonitorsCard(2), "aux")

    def test_get_sdgain(self):
        sd = DEVICE.servoDesign
        sd.clear()
        self.assertEqual(c.getSDGain(), 1)
        sd.gain = 0.5
        self.assertEqual(c.getSDGain(), 0.5)

    def test_get_lock_string(self):
        SERVO(1).lockGreater = 1
        SERVO(1).lockThreshold = 9
        SERVO(1).lockThresholdBreak = 9
        SERVO(1).relock = 1
        SERVO(1).lockRampmode = 0
        SERVO(1).lockSearch = 1
        self.assertEqual(c.getLockString(1), "search 1 ramp 0 relock 1 locked 0")
        SERVO(1).lockGreater = 0
        self.assertEqual(c.getLockString(1), "search 0 ramp 0 relock 1 locked 1")
        SERVO(1).lockRampmode = 1
        self.assertEqual(c.getLockString(1), "search 1 ramp 1 relock 1 locked 0")
        with self.assertRaises(TypeError):
            c.getLockString("hello")
        with self.assertRaises(ValueError):
            c.getLockString(10)

    def test_call_apply_filter_values(self):
        sd = DEVICE.servoDesign
        sd.clear()
        sd.integrator(5)
        sd.get(0).enabled = True
        sd.notch(100, 0.4)
        sd.get(1).enabled = False
        sd.notch(200)
        sd.get(2).enabled = True
        with self.assertRaises(PreventUpdate):
            c.callApplyFiltersToServo(1, 3, 2)
        with self.assertRaises(PreventUpdate):
            c.callApplyFiltersToServo(1, 1, None)
        values, _, _ = c.callApplyFiltersToServo(1, 1, 1)
        self.assertIn(0, values)
        values, _, _ = c.callApplyFiltersToServo(1, 1, 1)
        self.assertIn(2, values)
        sd.clear()
        values, _, _ = c.callApplyFiltersToServo(1, 1, 1)
        self.assertEqual(values, [])
        _, _, clicks = c.callApplyFiltersToServo(3, 3, 1)
        self.assertEqual(1, clicks)

    def test_handle_filter(self):
        sd = DEVICE.servoDesign
        sd.clear()

        _callbacks._handleFilter("Notch", 5, 0.4, True, 0)
        with self.assertRaises(PreventUpdate):
            _callbacks._handleFilter("Notch", None, 0.4, True, 0)
        with self.assertRaises(PreventUpdate):
            _callbacks._handleFilter("Notch", "samaralala", 0.4, True, 0)
        with self.assertRaises(PreventUpdate):
            _callbacks._handleFilter("Atomkraft nein danke!", 100, 0.5, True, 0)
        with self.assertRaises(PreventUpdate):
            _callbacks._handleFilter(
                "Notch", 1, "tempolimit auf deutschen autobahnen jetzt!", True, 0
            )
        _callbacks._handleFilter("Notch", 5, None, False, 0)
        self.assertFalse(sd.get(0).enabled)

    def test_call_filter_field(self):
        sd = DEVICE.servoDesign
        sd.clear()
        sd.integrator(5)
        sd.notch(200, 0.15)
        _ = c.callFilterField("Lowpass", 10, None, True, 0)
        with self.assertRaises(PreventUpdate):
            c.callFilterField("Lowpass", 2, 0, False, 0)
        self.assertIn("LP2", sd.get(0).description)
        _ = c.callFilterField("", "lala", 12, False, 0)
        self.assertIsNone(sd.get(0))

    def test_call_filter_visible(self):
        self.assertEqual(c.callFilterVisible("")[0], {"display": "none"})
        self.assertEqual(c.callFilterVisible("Notch")[1], {"display": "inline-block"})
        self.assertEqual(c.callFilterVisible(1234)[2], {"display": "inline-block"})

    def test_call_get_filter_options(self):
        log.warning(c.getFilterOptions())
        names = map(lambda x: x.__name__, c.getFilterOptions())
        self.assertIn("Notch", names)
        self.assertNotIn("afd", names)  # afd is out

    def test_call_lock_threshold_info(self):
        SERVO(2).lockGreater = 1
        SERVO(2).lockThreshold = 8
        SERVO(2).lockThresholdBreak = 8
        self.assertEqual(
            c.callLockThresholdInfo(None, 2), "Threshold >8.00 V (Break 8.00 V)"
        )
        SERVO(2).lockGreater = 0
        # should not change anything in testmode
        if "Mock" in str(type(SERVO(2)._adw)):
            self.assertIn("<8", c.callLockThresholdInfo(100, 2))

    def test_call_lock_ramp(self):
        context = MockContext("lock_amplitude_lock_offset")
        SERVO(2).lockFrequency = 5
        self.assertEqual(
            c.callLockRamp(3, 2, 5, context, 2),
            f"Amplitude {3:.2f} V | Offset {2:.2f} V | Frequency {5:.2f} Hz",
        )
        context = MockContext("lock_freq")
        # we expect some slight conversion issues on the frequency
        self.assertEqual(
            c.callLockRamp(None, None, 3, context, 2),
            f"Amplitude 3.00 V | Offset 2.00 V | Frequency 3.00 Hz",
        )
        SERVO(2).lockFrequency = 20
        SERVO(2).lockOffset = 0
        SERVO(2).lockAmplitude = 10
        context = MockContext("")
        self.assertEqual(
            c.callLockRamp(None, None, None, context, 2),
            f"Amplitude 10.00 V | Offset 0.00 V | Frequency 20.00 Hz",
        )

    def test_call_adwin_monitor(self):
        with self.assertRaises(PreventUpdate):
            _ = c.callADwinMonitor(1, None, "aux")
        with self.assertRaises(PreventUpdate):
            _ = c.callADwinMonitor(1, 2, None)
        with self.assertRaises(PreventUpdate):
            _ = c.callADwinMonitor(1, None, None)
        _ = c.callADwinMonitor(2, 2, "input")
        self.assertEqual(DEVICE.monitors[1]["servo"], 2)
        self.assertEqual(DEVICE.monitors[1]["card"], "input")

    def test_get_lock_button_label(self):
        SERVO(2).lockSearch = 0
        SERVO(2).locked = 0
        self.assertEqual(c.getLockButtonLabel(2), "Turn on")
        # make sure it does not find a lock
        SERVO(2).lockThreshold = 9
        SERVO(2).lockThresholdBreak = 9
        SERVO(2).lockGreater = True
        # start search
        SERVO(2).lockSearch = 1
        self.assertEqual(c.getLockButtonLabel(2), "Turn off")
