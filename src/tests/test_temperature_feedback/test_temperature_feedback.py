# pylin: disable=protected-access
import os
import unittest
from time import sleep

from openqlab import io
from websocket import create_connection

from nqontrol import ServoDevice
from nqontrol.general import settings
from nqontrol.servo._feedbackController import FeedbackController


@unittest.skipIf(
    os.environ.get("RUN_TEMP_TESTS", False) == False,
    "Run temperature tests only if wanted.",
)
class TestTemperatureFeedback(unittest.TestCase):
    def setUp(self):
        self.sd = ServoDevice(settings.DEVICE_NUM)
        self.sd.reboot()
        self.s = self.sd.servo(2)
        self.fc = FeedbackController(
            self.s, 1, (1, 2), 4, settings.DEFAULT_TEMP_HOST, settings.DEFAULT_TEMP_PORT
        )

    def test_tempFeedbackConnection(self):
        message = "mtd:1,2:3"

        s = create_connection(f"ws://127.0.0.1:5917")
        s.send(message)
        answer = s.recv()
        s.close()

        self.assertIn("OK", answer)
        self.assertIn("23.0°C", answer)

    def test_sendMessage(self):
        answer = self.fc._send(3)
        self.assertIn("OK.", answer)

    def test_calculateFeedback(self):
        dT = self.fc._calculateFeedback(10)
        self.assertAlmostEqual(dT, 1)

    def test_conditions(self):
        self.assertTrue(self.fc._checkConditions())
        self.s.enableRamp(frequency=1, amplitude=1)
        self.assertFalse(self.fc._checkConditions())

    def test_last_output(self):
        self.assertEqual(self.fc._last_output(), 0)
        self.s.enableRamp(frequency=50, amplitude=10)
        self.assertNotEqual(self.fc._last_output(), 0)
        out = []
        for _ in range(100):
            out.append(self.fc._last_output())
        print(out)
        self.assertGreater(max(out), 5)
        self.assertLess(min(out), -5)

    def test_threading(self):
        self.s.tempFeedbackStart(10, (1, 2), 0)
        sleep(0.2)
        self.assertIn("OK.", self.s._tempFeedback.last_answer)
        self.s.tempFeedbackStop()

    def test_start_without_parameters(self):
        self.s.tempFeedbackStart()
        sleep(0.2)
        self.s.tempFeedbackStop()

    def test_adding_plant_data_string(self):
        tests_dir = os.path.dirname(os.path.abspath(__file__))
        testfile = "{}/../support_files/fra.csv".format(tests_dir)
        compare = io.read(testfile)

        # file is encoded in 'cp1252'
        with open(testfile, mode="rb") as file:
            data_string = file.read().decode("cp1252")

        data = io.reads(data_string)

        self.assertTrue(data.equals(compare))
