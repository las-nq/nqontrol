import os
import unittest

import nqontrol
from nqontrol.general import settings


class TestConfiguration(unittest.TestCase):
    def test_run_develop_version(self):
        if "site-packages" in nqontrol.__path__[0]:
            raise Exception("Not running the development code!!!")

    def test_user_config(self):
        RUNNING_IN_DOCKER = os.environ.get("RUNNING_IN_DOCKER", False)
        if RUNNING_IN_DOCKER:
            self.assertEqual(settings.DEVICE_NUM, 0)
            self.assertEqual(settings.SETTINGS_FILE, "testing.json")
            self.assertEqual(settings.RAMP_FREQUENCY_MAX, 101)
