# pylint: disable=missing-class-docstring,missing-function-docstring
import unittest

import numpy as np

from nqontrol.general import helpers, settings


class TestBitManipulation(unittest.TestCase):
    def test_manipulate(self):
        x = 42
        y = helpers.changeBit(x, 2, True)
        self.assertEqual(y, 46)
        self.assertEqual(helpers.testBit(x, 2), 0)
        self.assertEqual(helpers.readBit(x, 2), False)
        self.assertEqual(helpers.testBit(y, 2), 1)
        self.assertEqual(helpers.readBit(y, 2), True)

        y = helpers.changeBit(y, 2, True)
        self.assertEqual(y, 46)
        self.assertEqual(helpers.testBit(y, 2), 1)
        self.assertEqual(helpers.readBit(y, 2), True)

        y = helpers.changeBit(y, 2, False)
        self.assertEqual(y, 42)
        self.assertEqual(helpers.testBit(y, 2), 0)
        self.assertEqual(helpers.readBit(y, 2), False)

        y = helpers.changeBit(y, 2, False)
        self.assertEqual(y, 42)
        self.assertEqual(helpers.testBit(y, 2), 0)
        self.assertEqual(helpers.readBit(y, 2), False)


class TestVoltConvertion(unittest.TestCase):
    def test_volt2int(self):
        self.assertIsInstance(helpers.convertVolt2Int(3.4), int)
        self.assertIsInstance(helpers.convertVolt2Int([3.4, 3]), np.ndarray)
        self.assertIsInstance(helpers.convertVolt2Int([3.4, 3])[0], np.int64)

        mode = 0
        self.assertAlmostEqual(helpers.convertVolt2Int(10, mode, signed=True), 0x7FFF)
        self.assertAlmostEqual(helpers.convertVolt2Int(10, mode, False), 0xFFFF)
        self.assertAlmostEqual(
            helpers.convertVolt2Int(3.7, mode, signed=True), round(0x8000 * 0.37), 0
        )
        self.assertAlmostEqual(helpers.convertVolt2Int(0, mode, signed=True), 0)
        self.assertAlmostEqual(helpers.convertVolt2Int(0, mode, signed=False), 0x8000)
        self.assertAlmostEqual(helpers.convertVolt2Int(-10, mode, signed=True), -0x8000)
        self.assertAlmostEqual(helpers.convertVolt2Int(-10, mode, signed=False), 0)
        # out of range
        self.assertAlmostEqual(helpers.convertVolt2Int(-20, mode, False), 0)
        self.assertAlmostEqual(helpers.convertVolt2Int(20, mode, False), 0xFFFF)

        mode = 3
        self.assertAlmostEqual(helpers.convertVolt2Int(1.25, mode, True), 0x7FFF)
        self.assertAlmostEqual(
            helpers.convertVolt2Int(0.4625, mode, True), round(0x8000 * 0.37), 0
        )
        self.assertAlmostEqual(helpers.convertVolt2Int(0, mode, True), 0)
        self.assertAlmostEqual(helpers.convertVolt2Int(-1.25, mode, True), -0x8000)
        # out of range
        self.assertAlmostEqual(helpers.convertVolt2Int(5, mode, False), 0xFFFF)
        self.assertAlmostEqual(helpers.convertVolt2Int(-5, mode, False), 0)

        # DataFrame

    def test_volt2float(self):
        self.assertIsInstance(helpers.convertVolt2Float(2.23), float)
        _ = helpers.convertVolt2Float(np.array([[1, 2, 3], [2, 3, 4]]))
        with self.assertRaises(TypeError):
            helpers.convertVolt2Float("It's rough and coarse...")

    def test_float2volt(self):
        mode = 0
        self.assertAlmostEqual(helpers.convertFloat2Volt(0x10000, mode), 10)
        self.assertAlmostEqual(helpers.convertFloat2Volt(-0x8000, mode), -10 - 10)
        self.assertAlmostEqual(helpers.convertFloat2Volt(0, mode), -10)
        self.assertAlmostEqual(helpers.convertFloat2Volt(0x8000 * 0.68, mode), 6.8 - 10)

        mode = 3
        self.assertAlmostEqual(helpers.convertFloat2Volt(0x8000, mode), 1.25 - 1.25)
        self.assertAlmostEqual(helpers.convertFloat2Volt(-0x8000, mode), -1.25 - 1.25)
        self.assertAlmostEqual(helpers.convertFloat2Volt(0, mode), 0 - 1.25)
        self.assertAlmostEqual(
            helpers.convertFloat2Volt(0x8000 * 0.68, mode), 0.85 - 1.25
        )

        # test with array
        self.assertListEqual(
            list(helpers.convertFloat2Volt([0, 0x8000, 0x8000 * 2])), [-10, 0, 10]
        )


class TestFrequencyConversion(unittest.TestCase):
    def test_convertFrequency2Stepsize(self):
        settings.SAMPLING_RATE = 200e3
        self.assertEqual(helpers.convertFrequency2Stepsize(0.001), 2e-08)
        self.assertEqual(helpers.convertFrequency2Stepsize(1e9), 20000)
        self.assertEqual(helpers.convertFrequency2Stepsize(127), 0.00254)

    def test_convertStepsize2Frequency(self):
        settings.SAMPLING_RATE = 200e3
        self.assertEqual(helpers.convertStepsize2Frequency(1), 50000)
        self.assertEqual(helpers.convertStepsize2Frequency(0.01), 500)
        self.assertEqual(helpers.convertStepsize2Frequency(2e-04), 10)

    def test_convertBack(self):
        for n in np.linspace(0, 1, num=1000):
            self.assertAlmostEqual(
                helpers.convertFrequency2Stepsize(helpers.convertStepsize2Frequency(n)),
                n,
                places=9,
            )
