# To use custom settings copy this sample to settings_local.py

#################### Local configuration ####################
# Use 0 to enable a dummy device. Otherwise use the channel number
DEVICES_LIST = [0]  # The 0 is reserved to mock device for testing!
# DEVICES_LIST = [1]
# LOG_LEVEL = 'INFO'  # Possible alternatives: DEBUG, INFO, WARNING, ERROR, CRITICAL
SETTINGS_FILE = 'testing.json'
# SERVO_NAMES = dict({
#     1: 'Cavity',
#     3: 'Mode Cleaner',
# })
