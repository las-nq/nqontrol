import re
from time import sleep
import websockets
import asyncio
import logging as log
from nqontrol import settings


class MockMTD:
    def __init__(self, p, m):
        self.external_control = True
        self.temp_setpoint = 20


class MockServer:
    def _parse_message(self, message):
        # Message should be e.g.: 'mtd:1,3:2.84'
        try:
            init, mtd_str, dT = re.split(':', message)
            if init != 'mtd':
                raise ValueError()
            dT = float(dT)
            port_nr, mtd_nr = re.split(',', mtd_str)
            mtd = MockMTD(int(port_nr), int(mtd_nr))
        except Exception as e:
            log.warning(e)
            raise ValueError(f'Not a valid message: {message}')
        return mtd, dT

    def _handle_message(self, message):
        try:
            mtd, dT = self._parse_message(message)
            if mtd.external_control:
                temp = mtd.temp_setpoint
                mtd.temp_setpoint = temp + dT
                sleep(.1)
                answer = f'OK. Temperature changed to {temp+dT}°C.'
                log.info(f"> {answer}")
            else:
                answer = 'External control is not enabled for this mtd.'
        except ValueError as e:
            log.warning(e)
            answer = f'Not a valid message: {message}\n{e}'
        return answer

    def feedback_server_start(self):
        @asyncio.coroutine
        def server(websocket, path):
            recv = yield from websocket.recv()
            log.info(f"< {recv}")
            answer = self._handle_message(recv)
            log.warning(f"> {answer}")
            yield from websocket.send(answer)

        start_server = websockets.serve(server, '127.0.0.1', settings.DEFAULT_TEMP_PORT)

        self._async_loop = asyncio.get_event_loop()
        self._async_loop.run_until_complete(start_server)
        self._async_loop.run_forever()


if __name__ == '__main__':
    server = MockServer()
    server.feedback_server_start()
