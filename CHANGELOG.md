# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<details>
<summary> How to write the changelog </summary>

### Guiding Principles

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

### Types of changes

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

</details>

## [1.2.2-beta.0] – 2021-04-30

### Fixed
- The gain was changed if filters were switched in the GUI

## [1.2.1] – 2021-04-28 – Beta accepted

### Removed
- Some unnecessary warnings

## [1.2.1-beta.0] – 2021-04-27

### Fixed
- Some filters were not correctly applied.

### Added
- Another instance of `ServoDevice` can now be created without changing the internal state.  
  This can be useful if the servos are controlled by the GUI and a new terminal instance should be used to take measurement data.  
  For this feature the parameter `keep_state` has to be `True`.

## [1.2] – 2020-10-26 – Beta accepted

### Changed
- Updated `OpenQlab`

## [1.2.beta.1] – 2020-10-23 – Further fixes and improvements

### Fixed
* The limits of the frequency slider for the ramp were not loaded from the configurable settings.
* Some settings were not loaded from json.

### Changed
* The adbasic part is now compiled by the CI
* The TTL trigger can now be selected for individual channels.
  That was necessary due to the ability to ramp all channels at the same time.
  The TTL on the DIG I/O output has been removed.
* `Servo` has been refactored.

## [1.2.beta.0] – 2020-06-20 – Further autolock optimizations

### Added
* Ramp frequency limits can be changed in the local user settings.

### Changed
* More intuitive ramp and autolock UI
* Relock hysteresis improvements

## [1.2.alpha.0] – 2020-04-29 – Where the ramp has been merged to the Autolock

### Added  
* Automatic lock signal analysis for both UI and terminal. This will set lock threshold and threshold direction (maximum or minimum) automatically. For command line use the servo method `lockAnalysis()`.
* Due to the `frequency` changes below, you can now set `RAMP_FREQUENCY_MIN` and -`MAX` in the settings.
* Introduced a lockBuffer on ADwin to fix a bug with relocking. Gives the ADwin some time for filters to kick in before checking whether lock is broken.  

### Changed  
* Autolock now uses Amplitude and Offset instead of a search range.
* Autolock internals reworked, resolving some concerns around the previous `frequency` to `stepsize` convention. The old stepsize was mainly a reflection of a static ramp array used in the ADwin script.  The ramp now uses a float stepsize and always ramps from -1 to 1 V, scaled by the amplitude with an added offset. The `convertStepsize2Frequency` and `convertFrequency2Stepsize` helpers have been changed accordingly.

### Removed  
* Most legacy ramping features. Ramp is now a full feature of the autolock section.

### Developer changes  
* We moved from `pipenv` to `poetry`. Check out the _poetry_ docs [here](https://python-poetry.org/docs/). After installing poetry, use `poetry install` in the current branch.
* __Added__ two new errors, `AutoLockError` and `AutoLockAnalysisError`
* Moved all callbacks from a central controller module to `_callbacks` modules in each individual widget section. Now everything is where it belongs. The `controller.py` still exists, so this should not be a breaking change. It is mostly used in tests though and imports all the callback modules.

## [1.1] – 2020-03-17 – Where the paper was published  

Heyo! We published the paper. Find it [here](https://aip.scitation.org/doi/10.1063/1.5135873).

## [1.1rc1] – 2020-03-16 – Where we found some more bugs and get ready for paper release

Minor update.

### Changed  
- Updated installation instructions in the docs
- __Fixed__ some bugs in the related to the Second Order Section of the UI.

### Developer changes
- Some changes to `MockADwin`
- Improved some docstrings  

## [1.1b3] – 2020-03-06 – Where we clean house

We reorganized the package structure, moved lots of code around. It still feels like we're getting closer to a real `v1.0`.

### Changed  
- Improved the `README.md` a lot.
- Changed the folder structure:
  – Organized UI widgets in single files and separated UI related stuff from backend.
- Improved documentation.
- Small changes to gitlab related stuff.

### Developer changes  
- We are currently heavily changing backend stuff (doesn't affect the user too much), so we recommend reading the actual code changes on gitlab if you'd like to get involved. :) If you're just starting now though, good for you – what's in the past is in the past.

## [1.1b2] – 2020-02-28 – Where we do some follow-up work  

This is a minor update, improving some autolock related things, test related things and some refactoring. We aim at improving the general code-quality in the coming releases, also reorganizing some of the package structure, as it grew a little out of hand.

### Breaking
- __BREAKING__: Moved from using a `DEVICES_LIST` in the `settings.py`, UI and `controller.py` to using only one ADwin device. Please make sure to update your local settings file in accordance with the changes in the `settings_local.sample.py`

### Added
- Autolock documentation: https://las-nq-serv.physnet.uni-hamburg.de/python/nqontrol/features.html#autolock (Please note that this links to the latest version, so it might change in the future)
- Updated pictures of the UI in the docs

### Changed  
- Updated documentation

### Developer changes  
- __BREAKING__: Moved from using a `DEVICES_LIST` in the `settings.py`, UI and `controller.py` to using only one ADwin device. The list was a remainder from the project start, where we wanted to keep the UI scalable. This keeps the project much more simplistic and lean. This also removes the `deviceNumber` parameter from ALL callback functions!
- Added a gitlab bug template
- Improved gitlab CI
- Made pylint harsher
- Some changes to CSS of the UI
- Improved signatures of conversion function in `general.py`
- some test refactoring


## [1.1b1] – 2020-02-17 – Where we introduce automatic locking  

We added a first version of a working auto-lock! This allows you to automatically search for a resonance peak and activate the filters you've configured. Start up the UI and try it out in this first release version! This is still an experimental feature and will be prone to heavy changes in the coming releases. Full documentation will be added in the coming releases as well.

### Added  
- __Autolocking__
- some missing documentation

### Changed
- Upgraded from `openqlab = ">=0.1.10.1"` to `openqlab = ">=0.1.11.1"`, fixing another bug
- Specified pandas version to `pandas = "<1.0"`, fixing an issue introduced by 1.0

### Removed  
- Snapping implementation (an old, very basic autolock)

### Developer changes  
- __Added__ a `watch_tests.sh` shell script (see folder `bin`) to improve your life
- __Improved__ readability in the adbasic `filter_module_inc` as well as the `nqontrol.bas` when working on autolock implementation
- __Improved__ test coverage
- __Fixed__ some minor bugs

## [1.0.1] – 2019-12-30 – Where ServoDesign gets a fix.  

Basically the real `v1.0`  

### Changed
- Upgraded from `dash = "==1.0.2"` to `dash = "==1.7.0"`
- Upgraded from `openqlab = ">=0.1.9.8"` to `openqlab = ">=0.1.10.1"`
  – Changed how filters are added and removed, fixing a `ServoDesign` issue
  – Changed a couple of callbacks related to that, fixing a UI problem
- Upgraded from `plotly = "==4.0.0"` to `plotly = "==4.3.0"`

### Removed
- __Removed the temperature control feature from UI__, as it is not really part of the ADwin controller and will be reworked in the future.
- Removed `dash-daq` from resources

### Developer changes
- Pre-commit configuration.
- Black code formatting
- Fixed some host address mistakes
- Introduced `dependencies.py` as central access point for `dash` app object
- Improved some persistence related stuff. Most values will now be written and read directly to ADwin.
- Removed a lot of old `multiprocessing` stuff, which proved unnecessary.
- Introduced `pipenv` along with a `Pipfile` instead of `requirements`

## [1.0.0] – 2019-11-25 – First feature-complete-ish version.

The first listed version.
