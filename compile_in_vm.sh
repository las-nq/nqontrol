#!/bin/bash

# To compile the adwin binaries in VirtualBox, mount the `adbasic` path as volume and use an adapted version of the following batch script:

# compile.cmd with F: as mounted volume of the adbasic directory
# F:
# C:\ADwin\ADbasic\ADbasic_C.exe /M adwin-control.bas /SPII /P12 /O3
#

# If you need to change something, just make a local copy of this script containing `*.local.*` in the filename to exclude it from git.

vbox_path="C:\\Users\\Chris\\adwin"
vbox_compiler="$vbox_path\\compile.cmd"

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  echo "** Trapped CTRL-C"
  VBoxManage controlvm Win7 savestate
  exit 0
}

nice -n 19 VBoxManage startvm Win7 --type headless 2> /dev/null || VBoxManage startvm Win7 2> /dev/null

while true;
do
  inotifywait -e modify src/adbasic/*
  echo $vb
  VBoxManage guestcontrol Win7 --username Chris --password 'z' run --exe "$vbox_compiler"
  # rsync -u src/adbasic/nqontrol.TC1 src/nqontrol/
done
