# ReadMe

![pipeline status](https://git.physnet.uni-hamburg.de/las-nq/adwin-control/badges/master/pipeline.svg)
![coverage report](https://git.physnet.uni-hamburg.de/las-nq/adwin-control/badges/master/coverage.svg)

`NQontrol` is a python project aiming the replacement of analog PID controllers in the lab.
    
The project is a solution based on the ADwin real-time platform that is able to deliver in excess of 8 simultaneous locking loops running with 200 kHz sampling frequency, and offers five second-order filtering sections per channel for optimal control performance. 
This Python package, together with a web-based GUI, makes the system easy to use and adapt for a wide range of control tasks in quantum-optical experiments.

The source code can be found on our [GitLab](https://gitlab.rrz.uni-hamburg.de/las-nq/nqontrol)

## Documentation

For more information please read the online documentation:

* Current documentation of the [latest release](https://las-nq-serv.physnet.uni-hamburg.de/python/nqontrol)
* Current documentation of the [latest development version](https://las-nq-serv.physnet.uni-hamburg.de/python/nqontrol-stage)

## NQontrol Installation

For installation please refer to the [documentation page](https://las-nq-serv.physnet.uni-hamburg.de/python/nqontrol/install.html)
