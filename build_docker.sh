#!/usr/bin/env bash
set -e

project=nqontrol:lib

docker build -t $project -f Dockerfile_lib .
docker tag $project lasnq/$project
docker push lasnq/$project

project=nqontrol

docker build -t $project .
docker tag $project lasnq/$project
docker push lasnq/$project
