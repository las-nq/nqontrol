#!/usr/bin/env bash
set -e

mkdir -p $HOME/.cache/{pypoetry-podman,apt}
chcon -Rt svirt_sandbox_file_t $HOME/.cache/{pypoetry-podman,apt} || echo "Selinux not installed"
cache_volumes="\
    --volume=$HOME/.cache/pypoetry-podman:/root/.cache/pypoetry \
    --volume=$HOME/.cache/apt:/var/cache/apt \
    "

project=nqontrol:lib
podman build \
    $cache_volumes \
    --build-arg APT_CACHER=$APT_CACHER \
    -t lasnq/$project -f Dockerfile_lib .
podman push lasnq/$project

project=nqontrol:compiler
podman build \
    $cache_volumes \
    -t lasnq/$project -f Dockerfile_compiler .
podman push lasnq/$project

project=nqontrol
podman build \
    $cache_volumes \
    --build-arg PIP_CACHER=$PIP_CACHER \
    -t lasnq/$project .
podman push lasnq/$project
