#!/usr/bin/env bash
set -m  # enable job control

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  echo "** Trapped CTRL-C"
  kill %1 && echo 'mock server killed'
  exit 1
}
# make mypy src/nqontrol || exit 1
poetry run python3 src/tests/support_files/mock_server.py &
poetry run py.test -x --no-cov-on-fail --cov-report=html src/tests
result=$?
echo "RETURN $result"

kill %1 && echo 'mock server killed'

exit $result
