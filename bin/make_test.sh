#!/usr/bin/env bash
set -m  # enable job control

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  echo "** Trapped CTRL-C"
  kill %1 && echo 'mock server killed'
  exit 1
}
export PYTHONPATH=$PYTHONPATH:`pwd`/src
python3 src/tests/mock_server.py &
py.test -x --no-cov-on-fail --cov-report=html src/tests
result=$?
echo "RETURN $result"

kill %1 && echo 'mock server killed'

exit $result
