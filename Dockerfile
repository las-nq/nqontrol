FROM lasnq/nqontrol:lib
ENV PYTHONUNBUFFERED 1
ENV BUILD_PACKAGES pandoc sass rsync openssh-client build-essential python3-tk

WORKDIR /code

RUN apt-get update -y && apt-get install -y $BUILD_PACKAGES

# Install Python packages
RUN pip install --upgrade pip
ADD requirements.txt Makefile /code/
RUN make requirements

# Configure ADwin device
ENV adwin_ip_address 192.168.0.3
RUN /opt/adwin/sbin/adconfig add 1 TYPE net IP $adwin_ip_address
