# syntax = docker/dockerfile:experimental
FROM lasnq/nqontrol:compiler
ENV PYTHONUNBUFFERED 1
ENV BUILD_PACKAGES pandoc sass rsync openssh-client build-essential python3-tk linkchecker

WORKDIR /code

# This volume is discarded after the build process.
# It just reduces the image size without deleting the directory.
VOLUME ["/root/.cache/pypoetry"]
VOLUME ["/var/cache/apt"]

RUN apt-get update -y && apt-get install -y $BUILD_PACKAGES

# Cache pip packages if possible
ARG PIP_CACHER=""
RUN curl -s $PIP_CACHER -o /dev/null 2>&1 || export PIP_CACHER=''

RUN if [ "$PIP_CACHER" != '' ]; then  \
    echo "install with cache\n"; \
    pip install --no-cache -i $PIP_CACHER/root/pypi/+simple/ \
    --trusted-host $(echo $PIP_CACHER | sed 's/http[s]*:..\([^ ]*\):[0-9]*/\1/') poetry && \
    poetry config repositories.cache $PIP_CACHER/root/pypi/+simple/ && \
    poetry config repositories.cache.default true; \
    else echo "skip pip cache\n"; pip install --no-cache poetry; fi


# Install Python packages
RUN poetry config virtualenvs.create false

COPY pyproject.toml poetry.lock /code/
RUN poetry install -vvv --no-interaction

# Configure ADwin device
ENV adwin_ip_address 192.168.167.104
RUN /opt/adwin/sbin/adconfig add 1 TYPE net IP $adwin_ip_address
