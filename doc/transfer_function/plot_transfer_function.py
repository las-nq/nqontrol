
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

filename = 'adwin_transfer_function.csv'

gain = pd.read_csv(filename, comment='#', header=0, low_memory=False, usecols=[1, 3], index_col=0)
phase = pd.read_csv(filename, comment='#', header=0, low_memory=False, usecols=[1, 4], index_col=0)

print(gain.head())
fig = plt.figure()
plt.subplot(2, 1, 1)
plt.xscale('log')
plt.plot(gain)
plt.subplot(2, 1, 2)
plt.xscale('log')
plt.plot(phase)
plt.show()

with PdfPages('plot.pdf') as pdf:
    pdf.savefig(fig)
