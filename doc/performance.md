# System Performance and Measurements

## Output Bit Noise

The quantization that results from the digital analog converter can be a dominant noise source.
In this example a mode cleaner is locked via a piezo.
To be able to scan the cavity over the whole spectral range a high voltage amplifier is used with an amplification of about 20.
In this case the bit steps are a relevant factor, reduce the accuracy and increase the noise.

![quantization noise](_static/adwin_output.png)

Output system: ADwin → HV amplifier → mode cleaner piezo

Input system: Incoupling reflection → photo diode → Pound-Drever-Hall scheme → ADwin


## Phase Delay

TODO measurement
