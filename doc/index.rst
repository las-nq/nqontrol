========
NQontrol
========

This is the documentation of **NQontrol**.
NQontrol is a Python project aiming the replacement of analog PID controllers in the lab.

The publicated paper about this project is available `here <https://doi.org/10.1063/1.5135873>`_.
The preprint version of the paper describing can be found on `arXiv <https://arxiv.org/abs/1911.08824>`_.

Read the :doc:`Introduction <intro>` for more information on the system.

Contents
========

.. toctree::
  :maxdepth: 2
  :caption: Usage

  Introduction <intro>
  Installation <install>
  Basic Usage <usage>
  Graphical User Interface <gui>
  Configuration <configuration>
  More Features <features>

.. toctree::
  :maxdepth: 2
  :caption: System and more

  System Performance and Measurements <performance>
  Authors <authors>

.. toctree::
  :maxdepth: 2
  :caption: API Reference

  NQontrol <apidoc/nqontrol>

.. toctree::
  :maxdepth: 2
  :caption: Development

  Installation <dev_install>
  Configuration <dev_configuration>
  Graphical User Interface <dev_gui>
  Real-time System (partly outdated) <realtime_system>

