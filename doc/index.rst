========
nqontrol
========

This is the documentation of **NQontrol**.
NQontrol is a Python project aiming the replacement of analog PID controllers in the lab.

Read the :doc:`Introduction <intro>` for more information on the system.

Contents
========

.. toctree::
  :maxdepth: 2
  :caption: Usage

  Introduction <intro>
  Installation <install>
  Basic Usage <usage>
  Graphical User Interface <gui>
  Configuration <configuration>

.. toctree::
  :maxdepth: 2
  :caption: Development

  Api Documentation <apidoc>
  Development Documentation <documentation>

.. toctree::
  :maxdepth: 2
  :caption: System and more

  System Performance and Measurements <performance>
  Authors <authors>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
