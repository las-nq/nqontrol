# Configuration

There are two possibilities to use an user configuration.
The recommended version is to use the local configuration file in the home directory. For development it can be practically to override those settings with a local variant.

## User Configuration File

When running `nqontrol` for the first time a configuration file is created at the path:
`~/.nqontrol.py`

There it is possible to set some variables, e.g. the list of connected ADwin device numbers:
```python
DEVICES_LIST = [0, 1]  # 0 is a mock device for testing
```
It is also possible to set names for the used servos:
```python
SERVO_NAMES = {
    1: 'Cavity',
    3: 'Mode Cleaner',
}
```

## Development Configuration

The second approach is to use the `settings_local.py` file in the directory `src/nqontrol`.
The file is defined equally and everything defined here will overwrite the variable from the user configuration and the settings file.
The main goal of this configuration is to overwrite user settings for testing on a system that is used for experimental control and development.
