# Configuration

There are two possibilities to use a user configuration.
The recommended version is to use the local configuration file in the home directory. For development it can be practically to override those settings with a local variant (see [development configuration](dev_configuration)).

## User Configuration File

When running `nqontrol` for the first time a configuration file is created at the path:
`~/.nqontrol.py`

There it is possible to set some variables, e.g. the index of connected ADwin device:
```python
DEVICE_NUM = 0  # 0 is a mock device for testing
```

When changing the host ip to `0.0.0.0` it will be accessible from the outside.
You can also change the used port.
```python
HOST = "127.0.0.1"
PORT = 8000
```

NQontrol will try to read the parameters for the servos from this file:
```python
SETTINGS_FILE = 'test.json'
```

When the backup creation is enabled it will create a copy with timestamp each time a json file is saved.
```python
CREATE_SETTINGS_BACKUP = False
BACKUP_SUBSTRING = "%Y-%m-%d_%H-%M-%S"
```

More information can be enabled using these parameters:
```python
DEBUG = True
LOG_LEVEL = 'INFO'
```
Please note that the UI will be much less performant in DEBUG mode. For normal use we recommend to leave this `False`.

If not many servos are used it might be worthwile to decrease the number of servos, hence it will reduce the startup time for the GUI.
```python
NUMBER_OF_SERVOS = 8
NUMBER_OF_MONITORS = 8
```
(We are working on implementing dynamic UI components.)
