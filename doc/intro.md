# Introduction

`NQontrol` is a python project aiming the replacement of analog PID controllers in the lab.


## System Description

A `nqontrol.ServoDevice` is a model of the whole ADwin device.
Each device can control eight servos modeled by `nqontrol.Servo`.
The device has a real time program running on the internal CPU.
It can be controlled by using this software package over a LAN connection.

![Servo device](_static/servo_device.svg)

A `Servo` has an input for the error signal, an aux input and an output for the controlling signal.
The signal can be manipulated by the gain, offset and five filters (second order sections).
Additionally an aux signal can be added to make a system analysis.

![Servo principle](_static/servo_scheme.svg)

The real time program runs with a sampling rate of 100kHz which can be increased if needed.
That implies that the captured frequency has an upper limit of a little less than 50kHz.
But the phase shift gets too large for frequencies of more than about 10 till 15kHz.
Therefore, that is the highest frequency that can be used for real life.

Digitalization of an analog signal with higher frequencies than half of the sampling rate produces an [aliasing effect](https://en.wikipedia.org/wiki/Aliasing) that must be avoided using an analog low pass filter which should cut ≤50kHz.
Other experiences concerning the optimization will follow.


## Hardware Configuration

It uses an [ADwin Pro II](https://www.adwin.de/de/produkte/proII.html) device with the following configuration:

* 1 Pro **CPU T12** 1GHz
* 2 **analog inputs** with *symmetric* Lemo cables (8 channels, 16bit and ±10V)
* 2 **analog outputs** with *asymmetric* Lemo cables (8 channels, 16bit and ±10V)
