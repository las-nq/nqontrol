nqontrol.gui.widgets.second\_order\_section package
===================================================

Submodules
----------

nqontrol.gui.widgets.second\_order\_section.filterWidget module
---------------------------------------------------------------

.. automodule:: nqontrol.gui.widgets.second_order_section.filterWidget
   :members:
   :undoc-members:
   :show-inheritance:

nqontrol.gui.widgets.second\_order\_section.sosHeaderWidget module
------------------------------------------------------------------

.. automodule:: nqontrol.gui.widgets.second_order_section.sosHeaderWidget
   :members:
   :undoc-members:
   :show-inheritance:

nqontrol.gui.widgets.second\_order\_section.sosWidget module
------------------------------------------------------------

.. automodule:: nqontrol.gui.widgets.second_order_section.sosWidget
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: nqontrol.gui.widgets.second_order_section
   :members:
   :undoc-members:
   :show-inheritance:
