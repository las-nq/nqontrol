nqontrol.gui.widgets.monitor\_section package
=============================================

Submodules
----------

nqontrol.gui.widgets.monitor\_section.adwinMonitorsWidget module
----------------------------------------------------------------

.. automodule:: nqontrol.gui.widgets.monitor_section.adwinMonitorsWidget
   :members:
   :undoc-members:
   :show-inheritance:

nqontrol.gui.widgets.monitor\_section.digitalOsciWidget module
--------------------------------------------------------------

.. automodule:: nqontrol.gui.widgets.monitor_section.digitalOsciWidget
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: nqontrol.gui.widgets.monitor_section
   :members:
   :undoc-members:
   :show-inheritance:
