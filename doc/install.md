# Installation

The whole architecture consists of three different software packages.
The most basic one is a C library that communicates with the ADwin device.
To make it available in Python there is a wrapper package `adwin` that is available via **pip**.

To bring the whole package to work, make sure, you have a `python3` installation with the package manager [pip](https://pypi.org/).

Then, you can install

* this `nqontrol` package
* and the `ADwin` library.


## Install NQontrol

### Using PyPi (recommended)

This is the recommended, easiest version to install NQontrol.
```bash
pip3 install --upgrade --user nqontrol
```
If you don't have `pip3` on your system, try `pip`.
To install system-wide don't use the `--user` flag.
    
### The Sources

Here you can get the newest stable version: 
**[gitlab repository](https://gitlab.com/las-nq/nqontrol)**


## Install the ADwin Library

### On Linux  

The ADwin connects via a fixed LAN connection, which has to be configured first.  
The library archive contains a documentation file which provides more details.
Download the library from the [ADwin file server](https://www.adwin.de/pub/cd/):
[adwin-lib-5.0.12](https://www.adwin.de/pub/cd/adwin-lib-5.0.12.tar.gz)

Compile and install the software:
```bash
./configure
make
sudo make install
```  

Configure the network of the ADwin device (if not done, yet):
```bash
adconfig config <MAC address> IP 192.168.0.2 MASK 255.255.255.0 
```

Next, connect the ADwin device using (change IP according to the one you assigned to your connection):

```bash
sudo /opt/adwin/sbin/adconfig add 1 UID username TYPE net IP 192.168.0.2
```

The `1` refers to the Device Number. For more information or in case this solution does no work for you, use:

```bash
man /opt/adwin/share/man/man8/adconfig.8
```

ADwin doesn't set the `PATH` variable.
If you want to have nicer commands without the full path you have to do it by yourself.
```bash
export PATH=$PATH:/opt/adwin/sbin
export MANPATH=$MANPATH:/opt/adwin/share/man
```
You can also add the commands to config files like `.profile`, `.zshenv`, etc...

### Windows

Generally, this package is not fully tested on Windows as of this moment. However, here's a few best practices to at least reduce errors:

* Gunicorn will not run on windows, since it is UNIX based. Thus, there is a Flask fallback in the `gunirun.py` file.
* First of all, make sure you've got `python` v3 correctly set up and on your path (we use the newest version). We do not use anaconda, so we recommend using `pip`.
* Use `pipenv` with `pip` to set up the app in a local environment. The installation guide for windows can be found here: https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv.
* Remove `gunicorn` in the `Pipfile` requirements!
* You will need the C++ build packages on Windows or you will get an error: Microsoft Visual C++ 14.0 is required. Get it with "Build Tools for Visual Studio": https://visualstudio.microsoft.com/downloads/ (Scroll down to "Tools for Visual Studio" and make sure you only get the Build Tools. Microsoft will ask you to download the full suite multiple times...)
* Run everything from a `pipenv shell`

If you manage to set everything up, please report any bugs, fix requests etc. to us on (https://gitlab.com/las-nq/nqontrol)! Maybe even write a description. :)
