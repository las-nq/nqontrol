# Installation

The whole architecture consists of three different software packages.
The most basic one is a C library that communicates with the ADwin device.
To make it available in Python there is a wrapper package `adwin` that is available via **pip**.

To bring the whole package to work, make sure, you have a `python3` installation with the package manager [pip](https://pypi.org/).

Then, you can install

* this `nqontrol` package
* and the `ADwin` library.

## Install NQontrol

### Using PyPi (recommended)

This is the recommended, easiest version to install nqontrol.
```bash
pip3 install --upgrade --user nqontrol
```
If you don't have `pip3` on your system, try `pip`.
To install system-wide don't use the `--user` flag.
    
### Other Sources

Here you can get the newest stable version: 

* **[gitlab repository](https://gitlab.rrz.uni-hamburg.de/las-nq/nqontrol)**

More distribution methods will follow.

#### Install Dependencies for Development

To install the dependencies on a Unix system, use:
```bash
make requirements      # to install in a virtualenv
make requirements-user # to install on user level
```
The general version for the only non-Unix system is:
```bash
pip install --user -r requirements.txt
```

#### Install NQontrol

The pythonic installation to the users site packages is done with:
```bash
python3 setup.py install --user
```

## Install the ADwin Library

### On Linux  

The ADwin connects via a fixed LAN connection, which has to be configured first.  
Download the library from the [ADwin file server](https://www.adwin.de/pub/cd/):

[adwin-lib-5.0.12](https://www.adwin.de/pub/cd/adwin-lib-5.0.12.tar.gz)

TL;DR:
```bash
./configure
make
sudo make install
```  

Install the most recent ADwin python3 library using

```bash
pip3 install --user adwin
```

Next, connect the ADwin device using (change IP according to the one you assigned to your connection):

```bash
sudo /opt/adwin/sbin/adconfig add 1 UID username TYPE net IP 192.168.0.2
```

The `1` refers to the Device Number. For more information or in case this solution does no work for you, use:

```bash
man /opt/adwin/share/man/man8/adconfig.8
```

ADwin doesn't set the `PATH` variable.
If you want to have nicer commands without the full path you have to do it by yourself.
```bash
export PATH=$PATH:/opt/adwin/sbin
export MANPATH=$MANPATH:/opt/adwin/share/man
```
You can also add the commands to config files like `.profile`, `.zshenv`, etc...

### Windows

Please look on the [ADwin Website](https://www.adwin.de/de/download/cdrom.html) and maybe write these installation instructions :)
