# Graphical User Interface

In case you do not want to work from the command line, the package provides a GUI based on `dash` (<https://dash.plot.ly>) to work with. The different components of the GUI and how to use them shall be introduced and for those seeking an in-depth look at the system this guide provides a quick insight into the `adwinUI.py` module.

## Starting the GUI

The GUI runs with `gunicorn` (<https://gunicorn.org/>), which is included in the requirements.

### With the installed package

If you have installed the `nqontrol` package, you can start the GUI by executing the command
```bash
nqontrol
```
(Ensure to have the corresponding `bin` directory in your `PATH`. For a user installation on unix it is located at `~/.local/bin`.)

After starting the server you can see the GUI in your browser on the default address [localhost:8000](http://localhost:8000).

### Other ways

Generally, gunicorn needs an entry point, in this case we're using `gunirun.py`. You can start this using

```bash
python3 gunirun.py
```

when in the appropriate directory. (The package might provide a neater way in the future.)

Please note that `gunicorn` does not enter through a main method, instead one has to provide a `server` instance. Luckily, `gunicorn` works with `Flask`, which `Dash` ist based on, and one can simply pass the dash instance to gunicorn. The gunicorn-server can also be run manually, using   

```bash
gunicorn -c guniconfig.py.ini run:server
```

if you are starting it from `src/nqontrol`. `-c guniconfig.py.ini` passes the configuration file, and `run:server` references the `run.py` module which assigns the dash instance to `server`. For more information, please read the extensive documentation on <https://gunicorn.org/#docs>.

## How it looks

After starting the server you will see something like this, depending on how you defined your environment variables. The ADwin device we worked with comes with `16` analog inputs and `16` outputs which translate to a maximum of `8` _servos_ as we use them in our lab with an `input`, an `aux` and an `output` channel each. The other `8` channels can be used as physical monitors and _servo_ channels can be assigned to them using the Monitor section of the GUI.

![GUI](_static/entry.png)  

The GUI will dynamically create as many components as you need. E.g. if you changed the `settings.NUMBER_OF_SERVOS` variable it will provide input sections accordingly.  

If you are running the GUI with multiple devices (which might be implemented in the future), the GUIs are just displayed one after the other. So each GUI comes with its own sections etc...  

### Header Section

![Header](_static/header.png)  

The header section consists of the _device name_, display ADwin's _workload and timestamp_ and provides _save_ and _reboot_ functions. If you provided a save file in your `settings_local.py` or `~/.nqontrol.py` (see section [configuration](configuration.html)) its name will be displayed in the _input field_ by default. Assign the ADwin `ramp` using the `Radio Items` on the left. The _ramp_ can only be active on one channel at a time.  

All of the components in this section are implemented as part of the `adwinUI.UIDevice` class.  

### Servo Section

![Servo Section](_static/servosection.png)  

The Servo Section corresponds to `adwinUI.UIServoSection` and is divided into two parts. The left-hand part is implemented within the component and the ramp section is implemented by `adwinUI.UIRamp`.  

The section is modeled after the way ADwin works. So, basically, the input is just the incoming signal. Switch it off and it won't show in the output. Same goes for the offset, which is applied directly to the input (and the gain after the offset). If you turn on the aux, the aux signal is added to the output (after the input). However, if you don't turn on the output, you won't see any of that. Genius!  

Note that if a ramp is active on the channel, the output will only show the ramp!  

Both offset and gain have to be actively submitted by pressing _Enter_.

The sensitivity options can be used to get more resolution on your signal. ADwin always sends 16 bit data, so if the signal is only in the -1 to 1 Volt interval you might want to set the input sensitivity to a higher value, e.g. 3 limits the output to 1.25 Volts, while keeping it at 0 would monitor the entire -10 to 10 Volt interval.

Now, let's get to the Servo Design section and shed some light on the FILTERS.  

_As of this version the Servo Section also includes a snapping functionality, which is basically a rudimentary auto lock function. The snap limit and direction can be set directly below the Offset/Gain settings. Snapping has to be manually activated, the checkbox is found in the OUTPUT section._

### Servo Design  

![Servo Design](_static/servodesign.png)  

The Servo Design takes a set of filters and transforms them into a second order section which can then be send to ADwin and applied to the INPUT signals.  

The numeric input in the top left sets the target channel when applying the Design.  

The Gain input has to be actively submitted by pressing _Enter_.  

### Monitor  

![Monitor](_static/monitor.png)  

Will only show the channels you selected of a single servo.  

The ADwin Monitor Channels box corresponds to the _physical outputs_ on the ADwin device, while the `dash` graph component is just a digital output.  

## Basic component structure

The main container for all other components is `adwinUI.UIDevice`. Each component will implement two features by default: a `layout` property and a `setCallbacks` method. The _layouts_ can be nested within each other, just as one would using normal HTML5/CSS. Generally, the layouts have to be created first and the callback method invoked afterwards. So each component first adds all its subcomponent layouts and then calls their `setCallbacks` method as part of its own. We'll have a more indepth look at this below.  

## Bootstrap

For layouting the GUI we use Bootstrap 4. Bootstrap let's one formulate a layout in terms of **columns** and **rows**. Please beware: Cols can only be children of Rows, so if you wanted to implement a Col within a Col, you would first have to embed another Row.  

You can responsively assign these via the `className` attributes of components, e.g. passing `col-2 col-sm-3 col-xl-6` to a container would make it fill 2 columns on the smallest screen (mobile), then scale up to 3 columns on _small_ screens and only once it hits _xl_ it would fill 6 columns.  

Bootstrap defines a couple of these tags (off the top of my hat `xs`, `sm`, `md`, `lg`, `xl` - where _xs_ is the default and does not have to be explicitly stated). The full bootstrap documentation can be found on <https://getbootstrap.com/> and we encourage you to read it if you'd like to add custom components to the GUI.  

The `adwinUI` module also implements two helper methods `Col` and `Row`, in order to make the Dash layouts a bit more readable. Anytime you find a `Col()` or `Row()` component it will be purely a `Div` container. However, most other components are also assigned 'row' or 'col' attributes in their classNames. **Please also note: the `Row()` method automatically adds the 'row' class to the className attribute, while the `Col()` method does not, as the variations of 'col' attributes have to be much more specific.**  

## Layout & callback basics  

Let's examine a few of the inner workings of the Dash UI using the header. The sections is organized in two rows:  

```python
Row(
    children=[
        # Device No. Picker
        html.H1('ADwin Device No. {}'.format(self._deviceNumber), className='col-auto col-sm-7 col-lg-auto align-self-center'),
        # Workload and timestamp
        Col(...
        )
    ],
    className='justify-content-start align-items-center'
),
Row(
    children=[
        # Ramp target
        Col(...
        ),
        # Save filename
        Col(...
        ),
        # Save Button
        Col(...
        ),
        # Reboot Button
        Col(...
        ),
        # Error message output
        # These are just callback targets, they won't be visible on the page
        dcc.Store(
            id='error_{}'.format(self._deviceNumber),
        ),
        dcc.Store(id='save_out_{}'.format(self._deviceNumber))
    ],
    className='justify-content-start align-items-center'
)
```  

Let's have a more detailed look at the ramp target part.  

```python
# Ramp target
Col(
  children=[
    Row(  # As mentioned, columns can only be children of rows
      children=[
        # The label is just a Div with some text in it
        Col('Ramp', className='col-2 align-self-center'),
        # This is one of the dash_core_components (imported as dcc)
        dcc.RadioItems(
          # The Off option is assigned individually, the rest of the labels are created using an inline for-loop. This method is used quite often as part of components. It doesn't format nicely, tho...
          options=[{'label': 'Off', 'value': False}] + [{'label': i, 'value': i} for i in range(1, self._numberOfServos + 1)],
          # Most components get their initial value like this
          # This is in order to enable loading from a save file
          # If none was provided, they will just have a default value (in this case, `False`)
          value=controller.getCurrentRampLocation(self._deviceNumber),
          # The HTML id. IDs are used in all callbacks
          id='rampTarget_{}'.format(self._deviceNumber),
          className='col-10',
          # These attributes enable assigning HTML classes to subcomponents, as RadioItems are a custom dash react component which consists of multiple HTML elements
          # In this case, the classnames are part of the Bootstrap library and just look better than the standard Dash styles
          inputClassName='form-check-input',
          labelClassName='form-check form-check-inline'
        )
      ]
    )
  ],
  className='col-12 col-md-6'
)
```  

Before moving on, let's also check out the callback for the `Radio Items` element. It consists of two parts. The callbacks are initiated in `UIDevice.__setDeviceCallbacks()`, which itself is invoked as part of `UIDevice.setCallbacks()`. The callback function is   

```python
# Callbacks for the device control, e.g. timestamp and workload.
def __setDeviceCallbacks(self):

  ...  # more callback definitions here

  # Each callback needs a target, which is assigned using the HTML IDs
  ramp_servo_target = 'rampTarget_{}'.format(self._deviceNumber)
  # This will be the function the callback is used with
  dynamically_generated_function = self.__createRampCallback()
  # The self._app is the Dash instance
  self._app.callback(
    # Remember the Store components above? The output goes to their data field
    Output('rampInfo_{}'.format(self._deviceNumber), 'data'),
    # Inputs, Outputs and States each take the target id and the relevant component field
    # Also notice that Inputs and States are part of lists, but Output is not.
    # Each Output can have multiple inputs and take as many data states as one wants, but a callback can only be assigned to one Output and one Output can also take only a single callback!!!
    [Input(ramp_servo_target, 'value')]
  )(dynamically_generated_function)  # I have no clue how this notation works, but it does

# Callback for the RAMP switch
# This is the target function we use as dynamically_generated_function above
def __createRampCallback(self):
  # This has to return the callback function, for the enigmatic syntax to work
  def callback(targetInput):  # We defined a single input for the callback, so this takes one parameter
  # In case you defined e.g. 3 inputs and 2 states you wanna pass
  # then the syntax would be callback(inp1, inp2, inp3, sta1, sta2)
    controller.callToggleRamp(targetInput, self._deviceNumber)  # All that's left to do is call the controller function
    # All implementation is done in the `controller.py` module
  return callback
```  
