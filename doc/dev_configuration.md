# Development Configuration

The default configuration happens in the `settings.py` file. If you'd like to use a different configuratino, we recommend using the a local settings file. Just duplicate the `settings_local.sample.py` and remove the `.sample` part. Implement your specific settings there. Check the `settings.py` for what's available.  
For some more useful examples, check the [configuration](configuration) section (e.g. changing number of servos, host address or device number).  
It can also be done using the `~/nqontrol.py` file, but generally the `settings_local.py` file in the directory `src/nqontrol` should be used.
BEWARE: Most `settings.py` options should be used carefully and are primally aimed at development cases.