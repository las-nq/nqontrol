# Setup a Development Environment

## Pipenv

Use [Pipenv](https://pipenv.readthedocs.io/en/latest/) to manage the development packages.
```bash
pipenv install --dev  # for initial installation
pipenv shell  # to work in the virtual environment
```

## Code Formatter

We use [`pre-commit`](https://pre-commit.com/#python) for automatic code formatting before committing.
It is automatically installed with the development packages.
The command to enable the hooks is:
```bash
pre-commit install
```

## EditorConfig

Please make sure to have an editor (or a plugin) that supports [EditorConfig](https://editorconfig.org/).
It will avoid many unnecessary formatting problems.

## PyTest

We are using [pytest](https://github.com/pytest-dev/pytest) to run our Python unittests.
It is a bit further simplified by the command:
```bash
make test
```

## Container

Currently we are using [`Docker` containers](https://www.docker.com/resources/what-container) with [docker-compose](https://docs.docker.com/compose/).
We are open to move to another container provider ([**podman**](https://podman.io/)), but for now it is rather new and the [podman-compose](https://github.com/containers/podman-compose) functionality is not able to replace docker-compose, yet.

## Version Numbering

For version numbering standards see [here](https://packaging.python.org/guides/distributing-packages-using-setuptools/?highlight=beta#standards-compliance-for-interoperability).
