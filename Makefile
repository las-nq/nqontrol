all: requirements test pylint doc bdist

requirements:
	@poetry install --no-dev

requirements-devel:
	@poetry install

doc:
	@cd doc; poetry run make html

bdist:
	@sass --update src/nqontrol/gui/assets:src/nqontrol/gui/assets
	make adbasic
	@export PYTHONPATH=`pwd`/src; poetry build

adbasic:
	@cd src/adbasic/; adbasic /O3 /M nqontrol.bas /IP/opt/adwin/share/adbasic/Inc/ /LP/opt/adwin/share/adbasic/Lib/ /SPII /P12
	@mv src/adbasic/nqontrol.TC1 src/nqontrol/servodevice/

test:
	@export PYTHONPATH=`pwd`/src; ./bin/make_test.sh

pylint:
	@poetry run pylint src/nqontrol

pydocstyle:
	@poetry run pydocstyle src/nqontrol

black:
	poetry run black src

mypy:
	poetry run mypy --show-error-codes --pretty src

clean:
	@rm -r build dist
	cd doc; make clean

.PHONY: test doc bdist all
