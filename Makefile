PIPFLAGS = -r requirements.txt


all: requirements test doc package

requirements:
	@pip3 install --upgrade $(PIPFLAGS)

requirements-user:
	@pip3 install --upgrade --user $(PIPFLAGS)

doc:
	@export PYTHONPATH=$PYTHONPATH:`pwd`/src; cd doc; make html

upload-doc:
	# @cd doc; make upload

sdist:
	@python3 setup.py sdist

exe:
	@python3 setup.py bdist_wininst --plat-name=win-amd64

bdist:
	@sass --update src/nqontrol/assets:src/nqontrol/assets
	python3 setup.py bdist_wheel

packages: sdist exe bdist

test:
	./bin/make_test.sh

test-server:
	@export PYTHONPATH=$PYTHONPATH:`pwd`/src
	python3 src/tests/mock_server.py

install-user:
	@python3 setup.py install --user

install-system:
	@python3 setup.py install

clean:
	@rm -r build dist
	cd doc; make clean

.PHONY: test doc package all
